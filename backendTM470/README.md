# backendTM470
TM470 Backend

Part of a full system to be delivered to the Open University as part of my end-of-degree project. 

## Requirements to be run
* Access to a mongo server and it must be declared in a /config/serverconfig.json file (to be read by config.js in the same folder) as an environment variable MONGODB_URI. That URI must have the user, password and database hostname (or IP) and PORT must have the port number.
* The MONGODB_URI value must be part of a list of json objects exported as serverconfig. One of them for development, one of them for tests. Tests can be the same mongoDB server but it must define a different database, one that is deleted and recreated with the tests. 
* a JWT_SECRET variable must be created for the Json Web Token framework (part of the login mechanism) it can be anything you want. 

## How to run it

1. Download it
2. From the folder created by the project run "npm install", this will install all of its dependencies. 
3. Run "npm test" to ensure everything is working. 
4. Run "npm start" to run the project, the window will advice you what's the IP address and port it is runnning. 

## Example content of a serverconfig.json file

For obvious reasons the username and passwords are fake

{
  "test": {
    "PORT": 3000,
    "MONGODB_URI": "mongodb://pepito:curedham@jtacluster-shard-00-00-dnhkx.mongodb.net:27017,jtacluster-shard-00-01-dnhkx.mongodb.net:27017,jtacluster-shard-00-02-dnhkx.mongodb.net:27017/jobappdbTest?ssl=true&replicaSet=JTAcluster-shard-0&authSource=admin",
    "JWT_SECRET": "asdasd1231asdasdas23lihljkhkjh"
  },
  
  "development": {
    "PORT": 3000,
    "MONGODB_URI": "mongodb://pepito:curedham@jtacluster-shard-00-00-dnhkx.mongodb.net:27017,jtacluster-shard-00-01-dnhkx.mongodb.net:27017,jtacluster-shard-00-02-dnhkx.mongodb.net:27017/jobappdb?ssl=true&replicaSet=JTAcluster-shard-0&authSource=admin",
    "JWT_SECRET": "ouiiosd1231aa122312113ihljkhhjk"
  }
}


