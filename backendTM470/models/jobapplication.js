var mongoose = require('mongoose');

//Date applied should be a date parametre at some point.
var JobApplication = mongoose.model('JobApplication', {
  notes: {
    type: String,
    required: false,
    minlength: 1,
    trim: true
  },
  roleName: {
    type: String,
    required: true,
    minlength: 1,
    trim: true
  },
  dateApplied: {
    type: String,
    required: false,
    minlength: 1,
    trim: true
  },
  companyName: {
    type: String,
    required: false,
    minlength: 1,
    trim: true
  },
  location: {
    type: String,
    required: false,
    minlength: 1,
    trim: true
  },
    distance: {
        type: String,
        required: false,
        minlength: 1,
        trim: true
    },
  referenceLetterName: {
    type: String,
    required: false,
    minlength: 1,
    trim: true
  },
  cvUsedName: {
    type: String,
    required: false,
    minlength: 1,
    trim: true
  },
  _creator: {
    type: mongoose.Schema.Types.ObjectId,
    required: false
  }
});

module.exports = {JobApplication};
