
require('./config/config');
const {mongoose} = require('./config/config');
const express = require('express');
const bodyParser = require('body-parser');


const jobapplications = require('./routes/jobapplications');
const users = require('./routes/users');
const fs = require('fs');
const fallback = require('express-history-api-fallback');


const app = express();
app.use('/api', express.static(__dirname + '/routes/doc'));
app.use('/', express.static(__dirname + '/views'));


app.use(bodyParser.json());

app.get('/makemebusy', function(req, res) {
  // processing the request requires some work!
  var i = 0;
  while (i < 10000000000) i++;
	  res.send("I counted to " + i);
});



// Get rid of bloody CORS
app.use(function (req, res, next) {

    // Website you wish to allow to connect
    res.setHeader('Access-Control-Allow-Origin', '*');

    // Request methods you wish to allow
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');

    // Request headers you wish to allow
    res.setHeader('Access-Control-Allow-Headers', 'x-auth, content-type');

    // Set to true if you need the website to include cookies in the requests sent
    // to the API (e.g. in case you use sessions)
    res.setHeader('Access-Control-Allow-Credentials', true);

    res.setHeader('Access-Control-Expose-Headers', 'x-auth, content-type');

    res.set('Access-Control-Request-Headers', 'x-auth, content-type');

    // Pass to next layer of middleware
    next();
});


app.use('/api/', jobapplications);
app.use('/api/', users);

//Remember the next on the call and as a function
app.use((req, res, next) => {
    const timestamp = new Date().toLocaleString('en-GB');
    const log = `Date/Time: ${timestamp}, Request: ${req.method}, Page: ${req.url}`;
    fs.appendFile('server.log', log + '\n', (err) => {
        if (err) {
            console.log('Unable to append to server.log')
        }
    });
	console.log(log);
    next();
});

app.use(fallback(__dirname + '/views/'));

const server = app.listen(process.env.PORT, () => {
    console.log(`TM470 Job Application Project server started at port ${process.env.PORT}`);
});

process.on('SIGINT', function() {
  server.close();
  // calling .shutdown allows your process to exit normally
  toobusy.shutdown();
  process.exit();
});

module.exports = {app};
