(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["main"],{

/***/ "./src/$$_lazy_route_resource lazy recursive":
/*!**********************************************************!*\
  !*** ./src/$$_lazy_route_resource lazy namespace object ***!
  \**********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncaught exception popping up in devtools
	return Promise.resolve().then(function() {
		var e = new Error('Cannot find module "' + req + '".');
		e.code = 'MODULE_NOT_FOUND';
		throw e;
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = "./src/$$_lazy_route_resource lazy recursive";

/***/ }),

/***/ "./src/app/app.component.css":
/*!***********************************!*\
  !*** ./src/app/app.component.css ***!
  \***********************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/app.component.html":
/*!************************************!*\
  !*** ./src/app/app.component.html ***!
  \************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<router-outlet></router-outlet>\n\n\n"

/***/ }),

/***/ "./src/app/app.component.ts":
/*!**********************************!*\
  !*** ./src/app/app.component.ts ***!
  \**********************************/
/*! exports provided: AppComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppComponent", function() { return AppComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _user_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./user.service */ "./src/app/user.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var AppComponent = /** @class */ (function () {
    function AppComponent(userService, router) {
        this.userService = userService;
        this.router = router;
    }
    AppComponent.prototype.ngOnInit = function () {
        if (this.userService.checkLoggedIn() === false) {
            // Send to the signup page.
            this.router.navigate(['/login']);
        }
        else {
            // Log the user and
            this.userService.logUser();
            this.router.navigate(['/list']);
            // Send the user to the list page.
        }
    };
    AppComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-root',
            template: __webpack_require__(/*! ./app.component.html */ "./src/app/app.component.html"),
            styles: [__webpack_require__(/*! ./app.component.css */ "./src/app/app.component.css")]
        }),
        __metadata("design:paramtypes", [_user_service__WEBPACK_IMPORTED_MODULE_1__["UserService"], _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"]])
    ], AppComponent);
    return AppComponent;
}());



/***/ }),

/***/ "./src/app/app.module.ts":
/*!*******************************!*\
  !*** ./src/app/app.module.ts ***!
  \*******************************/
/*! exports provided: AppModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppModule", function() { return AppModule; });
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/platform-browser */ "./node_modules/@angular/platform-browser/fesm5/platform-browser.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var ngx_bootstrap_dropdown__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ngx-bootstrap/dropdown */ "./node_modules/ngx-bootstrap/dropdown/index.js");
/* harmony import */ var ngx_bootstrap_tooltip__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ngx-bootstrap/tooltip */ "./node_modules/ngx-bootstrap/tooltip/index.js");
/* harmony import */ var ngx_bootstrap_modal__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ngx-bootstrap/modal */ "./node_modules/ngx-bootstrap/modal/index.js");
/* harmony import */ var _app_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./app.component */ "./src/app/app.component.ts");
/* harmony import */ var _loginpage_loginpage_component__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./loginpage/loginpage.component */ "./src/app/loginpage/loginpage.component.ts");
/* harmony import */ var _page_not_found_page_not_found_component__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./page-not-found/page-not-found.component */ "./src/app/page-not-found/page-not-found.component.ts");
/* harmony import */ var _listpage_listpage_component__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ./listpage/listpage.component */ "./src/app/listpage/listpage.component.ts");
/* harmony import */ var _listpage_topbar_topbar_component__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ./listpage/topbar/topbar.component */ "./src/app/listpage/topbar/topbar.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};





// Bootstrap modules.








var appRoutes = [
    { path: 'login', component: _loginpage_loginpage_component__WEBPACK_IMPORTED_MODULE_9__["LoginpageComponent"] },
    { path: '', component: _app_component__WEBPACK_IMPORTED_MODULE_8__["AppComponent"] },
    { path: 'list', component: _listpage_listpage_component__WEBPACK_IMPORTED_MODULE_11__["ListpageComponent"] },
    { path: '**', component: _page_not_found_page_not_found_component__WEBPACK_IMPORTED_MODULE_10__["PageNotFoundComponent"] }
];
var AppModule = /** @class */ (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            declarations: [
                _app_component__WEBPACK_IMPORTED_MODULE_8__["AppComponent"],
                _loginpage_loginpage_component__WEBPACK_IMPORTED_MODULE_9__["LoginpageComponent"],
                _page_not_found_page_not_found_component__WEBPACK_IMPORTED_MODULE_10__["PageNotFoundComponent"],
                _listpage_listpage_component__WEBPACK_IMPORTED_MODULE_11__["ListpageComponent"],
                _listpage_topbar_topbar_component__WEBPACK_IMPORTED_MODULE_12__["TopbarComponent"]
            ],
            imports: [
                ngx_bootstrap_dropdown__WEBPACK_IMPORTED_MODULE_5__["BsDropdownModule"].forRoot(),
                ngx_bootstrap_tooltip__WEBPACK_IMPORTED_MODULE_6__["TooltipModule"].forRoot(),
                ngx_bootstrap_modal__WEBPACK_IMPORTED_MODULE_7__["ModalModule"].forRoot(),
                _angular_platform_browser__WEBPACK_IMPORTED_MODULE_0__["BrowserModule"],
                _angular_common_http__WEBPACK_IMPORTED_MODULE_4__["HttpClientModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_3__["RouterModule"].forRoot(appRoutes, { enableTracing: false }),
                _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormsModule"]
            ],
            providers: [],
            bootstrap: [_app_component__WEBPACK_IMPORTED_MODULE_8__["AppComponent"]]
        })
    ], AppModule);
    return AppModule;
}());



/***/ }),

/***/ "./src/app/appsettings.ts":
/*!********************************!*\
  !*** ./src/app/appsettings.ts ***!
  \********************************/
/*! exports provided: AppSettings */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppSettings", function() { return AppSettings; });
var AppSettings = /** @class */ (function () {
    function AppSettings() {
    }
    AppSettings.API_ENDPOINT = 'https://www.maikel.uk/project/api/';
    return AppSettings;
}());



/***/ }),

/***/ "./src/app/jobapplication.service.ts":
/*!*******************************************!*\
  !*** ./src/app/jobapplication.service.ts ***!
  \*******************************************/
/*! exports provided: JobapplicationService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "JobapplicationService", function() { return JobapplicationService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _appsettings__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./appsettings */ "./src/app/appsettings.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var JobapplicationService = /** @class */ (function () {
    function JobapplicationService(http) {
        this.http = http;
        this.headerJson = {
            'Content-Type': 'application/json',
        };
    }
    JobapplicationService.prototype.getApplications = function (aToken) {
        this.inserToken(aToken);
        return this.http.get(_appsettings__WEBPACK_IMPORTED_MODULE_2__["AppSettings"].API_ENDPOINT + 'jobapplications', { headers: this.headerJson, observe: 'response' });
    };
    JobapplicationService.prototype.inserToken = function (aToken) {
        this.headerJson['x-auth'] = aToken;
    };
    JobapplicationService.prototype.sendJA = function (aJobApp, aToken) {
        this.inserToken(aToken);
        return this.http.post(_appsettings__WEBPACK_IMPORTED_MODULE_2__["AppSettings"].API_ENDPOINT + 'jobapplications', aJobApp, { headers: this.headerJson, observe: 'response' });
    };
    JobapplicationService.prototype.amendJA = function (aJobApp, aToken) {
        this.inserToken(aToken);
        return this.http.patch(_appsettings__WEBPACK_IMPORTED_MODULE_2__["AppSettings"].API_ENDPOINT + 'jobapplications/' + aJobApp._id, aJobApp, { headers: this.headerJson, observe: 'response' });
    };
    JobapplicationService.prototype.getDistance = function (aLocation, aUserPosctode) {
        return this.http.get('https://maps.googleapis.com/maps/api/distancematrix/json?units=metric&origins=56+Brouncker+Road, W38AQ&destinations=8 Elgin Avenue+W38EL&key=AIzaSyCvk_e8MCvFdvy4Xi8l1jYsWXiedbefNUk', { headers: this.headerJson, observe: 'response' });
    };
    JobapplicationService.prototype.deleteJA = function (aJobApp, aToken) {
        this.inserToken(aToken);
        return this.http.delete(_appsettings__WEBPACK_IMPORTED_MODULE_2__["AppSettings"].API_ENDPOINT + 'jobapplications/' + aJobApp._id, { headers: this.headerJson, observe: 'response' });
    };
    JobapplicationService = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])({
            providedIn: 'root'
        }),
        __metadata("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"]])
    ], JobapplicationService);
    return JobapplicationService;
}());



/***/ }),

/***/ "./src/app/jobapplication.ts":
/*!***********************************!*\
  !*** ./src/app/jobapplication.ts ***!
  \***********************************/
/*! exports provided: Jobapplication */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Jobapplication", function() { return Jobapplication; });
var Jobapplication = /** @class */ (function () {
    function Jobapplication(roleName, companyName, notes, location, dateApplied, referenceLetterName, cvUsedName, _creator, _id, distance) {
        this.roleName = roleName;
        this.companyName = companyName;
        this.notes = notes;
        this.location = location;
        this.dateApplied = dateApplied;
        this.referenceLetterName = referenceLetterName;
        this.cvUsedName = cvUsedName;
        this._creator = _creator;
        this._id = _id;
        this.distance = distance;
    }
    Jobapplication.parseJobApplications = function (body) {
        var formattedJobApps = [];
        var jobapps = body.jobapplications;
        for (var _i = 0, jobapps_1 = jobapps; _i < jobapps_1.length; _i++) {
            var someJAP = jobapps_1[_i];
            var anAPP = new Jobapplication(someJAP.roleName, someJAP.companyName, someJAP.notes, someJAP.location, someJAP.dateApplied, someJAP.referenceLetterName, someJAP.cvUsedName, someJAP._creator, someJAP._id, someJAP.distance);
            formattedJobApps.push(anAPP);
        }
        return formattedJobApps;
    };
    return Jobapplication;
}());



/***/ }),

/***/ "./src/app/listpage/listpage.component.css":
/*!*************************************************!*\
  !*** ./src/app/listpage/listpage.component.css ***!
  \*************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".bold {\n  font-weight: bold\n}\n\na {\n    color: inherit\n}\n\n.container {\n  background-color: white;\n}\n"

/***/ }),

/***/ "./src/app/listpage/listpage.component.html":
/*!**************************************************!*\
  !*** ./src/app/listpage/listpage.component.html ***!
  \**************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"container\">\n  <app-topbar [email]=\"theUser.email\" (doLogout)=\"logout()\"></app-topbar>\n <div class=\"page-header\">\n    <h1>Your applications<small> {{theUser.email}}</small></h1>\n  </div>\n  <button (click)=\"openWindow(template, aJobApp, 'CREATE')\" type=\"submit\" class=\"btn btn-primary\">Create Application</button>\n</div>\n<div class=\"container\">\n  <div class=\"row pull-right\">\n<p>Sort by:\n    <small><a [ngClass]=\"{'bold': isSortedByDate }\" (click)=\"sortByDate()\">Date</a></small>\n  <small> / </small>\n    <small><a [ngClass]=\"{'bold': !isSortedByDate }\" (click)=\"sortByDistance()\">Distance</a></small></p>\n  </div>\n  </div>\n<!--- The list of job applications -->\n<div class=\"container\">\n<table class=\"table\">\n  <thead>\n  <tr>\n    <th scope=\"col\">Role Name</th>\n    <th scope=\"col\">Company Name</th>\n    <th scope=\"col\">Date Applied</th>\n    <th scope=\"col\">Location</th>\n    <th scope=\"col\">Distance</th>\n  </tr>\n  </thead>\n  <tbody>\n  <tr *ngFor=\"let jobapp of theJAs\">\n    <th scope=\"row\" (click)=\"openWindow(template2, jobapp, 'AMEND')\">{{jobapp.roleName}}</th>\n    <td>{{jobapp.companyName}}</td>\n    <td>{{jobapp.dateApplied}}</td>\n    <td>{{jobapp.location}}</td>\n    <td>{{formatNice(jobapp.distance)}}</td>\n  </tr>\n  </tbody>\n</table>\n</div>\n\n<!--This is the modal window to create a job application-->\n<ng-template #template>\n  <!-- The header -->\n  <div class=\"modal-header\">\n    <h4 class=\"modal-title pull-left\">New Job Application</h4>\n    <button type=\"button\" class=\"close pul-right\" aria-label=\"Close\" (click)=\"modalRef.hide()\">\n      <span aria-hidden=\"true\">x</span>\n    </button>\n  </div>\n\n  <!-- The body with the form -->\n  <div class=\"modal-body\">\n\n      <div class=\"form-group\">\n        <label for=\"rolename\">Role Name</label>\n        <input type=\"text\" class=\"form-control\" [(ngModel)]='aJobApp.roleName' id=\"rolename\">\n      </div>\n\n      <div class=\"form-group\">\n        <label for=\"company\">Company</label>\n        <input type=\"text\" class=\"form-control\" [(ngModel)]='aJobApp.companyName' id=\"company\">\n      </div>\n      <div class=\"form-group\">\n        <label for=\"notes\">Notes:</label>\n        <textarea class=\"form-control\" [(ngModel)]='aJobApp.notes' rows=\"5\" id=\"notes\"></textarea>\n      </div>\n      <div class=\"form-group\">\n        <label for=\"location\">Location</label>\n        <input type=\"text\" class=\"form-control\" [(ngModel)]='aJobApp.location' id=\"location\">\n      </div>\n    <div class=\"form-group\">\n      <label for=\"dateApplied\">Date applied</label>\n      <input type=\"date\" class=\"form-control\" [(ngModel)]='aJobApp.dateApplied' id=\"dateApplied\">\n    </div>\n    <div class=\"form-group\">\n      <label for=\"referenceLetterName\">Reference Letter Name</label>\n      <input type=\"text\" class=\"form-control\" [(ngModel)]='aJobApp.referenceLetterName' id=\"referenceLetterName\">\n    </div>\n    <div class=\"form-group\">\n      <label for=\"cvUsedName\">CV Used</label>\n      <input type=\"text\" class=\"form-control\" [(ngModel)]='aJobApp.cvUsedName' id=\"cvUsedName\">\n    </div>\n    <button (click)=\"modalRef.hide()\" type=\"submit\" class=\"btn btn-danger\">Back</button>\n    <button (click)=\"sendJA()\" class=\"btn btn-primary pull-right\">Send</button>\n\n\n  </div>\n</ng-template>\n\n<!--This is the modal window to create a job application-->\n<ng-template #template2>\n  <!-- The header -->\n  <div class=\"modal-header\">\n    <h4 class=\"modal-title pull-left\">Modify Job Application</h4>\n    <button type=\"button\" class=\"close pul-right\" aria-label=\"Close\" (click)=\"modalRef.hide()\">\n      <span aria-hidden=\"true\">x</span>\n    </button>\n  </div>\n\n  <!-- The body with the form -->\n  <div class=\"modal-body\">\n\n    <div class=\"form-group\">\n      <label for=\"rolename\">Role Name</label>\n      <input type=\"text\" class=\"form-control\" [(ngModel)]='aJobApp.roleName' id=\"rolename\">\n    </div>\n\n    <div class=\"form-group\">\n      <label for=\"company\">Company</label>\n      <input type=\"text\" class=\"form-control\" [(ngModel)]='aJobApp.companyName'>\n    </div>\n    <div class=\"form-group\">\n      <label for=\"notes\">Notes:</label>\n      <textarea class=\"form-control\" [(ngModel)]='aJobApp.notes' rows=\"5\" ></textarea>\n    </div>\n    <div class=\"form-group\">\n      <label for=\"location\">Location</label>\n      <input type=\"text\" class=\"form-control\" [(ngModel)]='aJobApp.location'>\n    </div>\n    <div class=\"form-group\">\n      <label for=\"dateApplied\">Date applied</label>\n      <input type=\"date\" class=\"form-control\" [(ngModel)]='aJobApp.dateApplied'>\n    </div>\n    <div class=\"form-group\">\n      <label for=\"referenceLetterName\">Reference Letter Name</label>\n      <input type=\"text\" class=\"form-control\" [(ngModel)]='aJobApp.referenceLetterName'>\n    </div>\n    <div class=\"form-group\">\n      <label for=\"cvUsedName\">CV Used</label>\n      <input type=\"text\" class=\"form-control\" [(ngModel)]='aJobApp.cvUsedName'>\n    </div>\n    <button (click)=\"modalRef.hide()\" type=\"submit\" class=\"btn btn-danger\">Back</button>\n    <div class=\"btn-group pull-right\">\n\n    <button (click)=\"amendJA()\" class=\"btn btn-primary\">Change</button>\n    <button (click)=\"deleteJA()\" class=\"btn btn-primary\">Delete</button>\n    </div>\n  </div>\n</ng-template>\n"

/***/ }),

/***/ "./src/app/listpage/listpage.component.ts":
/*!************************************************!*\
  !*** ./src/app/listpage/listpage.component.ts ***!
  \************************************************/
/*! exports provided: ListpageComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ListpageComponent", function() { return ListpageComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _user_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../user.service */ "./src/app/user.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _jobapplication_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../jobapplication.service */ "./src/app/jobapplication.service.ts");
/* harmony import */ var _jobapplication__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../jobapplication */ "./src/app/jobapplication.ts");
/* harmony import */ var ngx_bootstrap__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ngx-bootstrap */ "./node_modules/ngx-bootstrap/index.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var ListpageComponent = /** @class */ (function () {
    function ListpageComponent(modalService, userService, router, jaService) {
        this.modalService = modalService;
        this.userService = userService;
        this.router = router;
        this.jaService = jaService;
        this.isSortedByDate = true;
        this.aJobApp = new _jobapplication__WEBPACK_IMPORTED_MODULE_4__["Jobapplication"]('', '', '', '');
    }
    ListpageComponent.prototype.ngOnInit = function () {
        this.getUser();
        this.getList();
    };
    ListpageComponent.prototype.getUser = function () {
        this.theUser = this.userService.getUser();
    };
    ListpageComponent.prototype.logout = function () {
        // Delete the user locally
        this.userService.logout();
        // Go back to AppComponent
        this.router.navigate(['/']);
    };
    ListpageComponent.prototype.getList = function () {
        var _this = this;
        this.jaService.getApplications(this.theUser.token)
            .subscribe(function (response) {
            _this.theJAs = _jobapplication__WEBPACK_IMPORTED_MODULE_4__["Jobapplication"].parseJobApplications(response.body);
            if (_this.isSortedByDate === true) {
                _this.sortByDate();
            }
            else {
                _this.sortByDistance();
            }
        });
    };
    ListpageComponent.prototype.openWindow = function (template, ajobapp, text) {
        if (text === 'CREATE') {
            this.aJobApp = new _jobapplication__WEBPACK_IMPORTED_MODULE_4__["Jobapplication"]('', '', '', '');
        }
        else {
            this.aJobApp = ajobapp;
        }
        this.modalRef = this.modalService.show(template);
    };
    ListpageComponent.prototype.sendJA = function () {
        var _this = this;
        this.jaService.sendJA(this.aJobApp, this.theUser.token)
            .subscribe(function (response) {
            _this.getList();
            _this.modalRef.hide();
        });
    };
    ListpageComponent.prototype.amendJA = function () {
        var _this = this;
        this.jaService.amendJA(this.aJobApp, this.theUser.token)
            .subscribe(function (response) {
            _this.getList();
            _this.modalRef.hide();
        });
    };
    ListpageComponent.prototype.formatNice = function (aDistance) {
        if (aDistance === '-1') {
            return 'n/a';
        }
        return Math.round(aDistance / 1000).toString() + ' kms';
    };
    ListpageComponent.prototype.sortByDate = function () {
        this.isSortedByDate = true;
        // This needs to be fixed
        this.theJAs.sort(function (a, b) {
            var left = (a.dateApplied === undefined ? new Date('2015-01-01') : a.dateApplied);
            var right = (b.dateApplied === undefined ? new Date('2015-01-01') : b.dateApplied);
            var result = new Date(right).getTime() - new Date(left).getTime();
            return result;
        });
    };
    ListpageComponent.prototype.sortByDistance = function () {
        this.isSortedByDate = false;
        this.theJAs.sort(function (a, b) {
            var left = parseInt(a.distance, 10);
            var right = parseInt(b.distance, 10);
            if (left > right) {
                return 1;
            }
            if (left < right) {
                return -1;
            }
            if (left === right) {
                return 0;
            }
        });
    };
    ListpageComponent.prototype.deleteJA = function () {
        var _this = this;
        this.jaService.deleteJA(this.aJobApp, this.theUser.token)
            .subscribe(function (response) {
            _this.getList();
            _this.modalRef.hide();
        });
    };
    ListpageComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-listpage',
            template: __webpack_require__(/*! ./listpage.component.html */ "./src/app/listpage/listpage.component.html"),
            styles: [__webpack_require__(/*! ./listpage.component.css */ "./src/app/listpage/listpage.component.css")]
        }),
        __metadata("design:paramtypes", [ngx_bootstrap__WEBPACK_IMPORTED_MODULE_5__["BsModalService"], _user_service__WEBPACK_IMPORTED_MODULE_1__["UserService"], _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"],
            _jobapplication_service__WEBPACK_IMPORTED_MODULE_3__["JobapplicationService"]])
    ], ListpageComponent);
    return ListpageComponent;
}());



/***/ }),

/***/ "./src/app/listpage/topbar/topbar.component.css":
/*!******************************************************!*\
  !*** ./src/app/listpage/topbar/topbar.component.css ***!
  \******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/listpage/topbar/topbar.component.html":
/*!*******************************************************!*\
  !*** ./src/app/listpage/topbar/topbar.component.html ***!
  \*******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<nav class=\"navbar navbar-default\">\n  <div class=\"container-fluid\">\n    <div class=\"navbar-header\">\n      <a class=\"navbar-brand\" href=\"#\">Job Aplication Tracker</a>\n    </div>\n    <ul class=\"nav navbar-nav navbar-right\">\n      <li class=\"navbar-btn navbar-text\" (click)=\"logout()\">Logout</li>\n    </ul>\n    </div>\n</nav>\n"

/***/ }),

/***/ "./src/app/listpage/topbar/topbar.component.ts":
/*!*****************************************************!*\
  !*** ./src/app/listpage/topbar/topbar.component.ts ***!
  \*****************************************************/
/*! exports provided: TopbarComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TopbarComponent", function() { return TopbarComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var TopbarComponent = /** @class */ (function () {
    function TopbarComponent() {
        this.doLogout = new _angular_core__WEBPACK_IMPORTED_MODULE_0__["EventEmitter"]();
    }
    TopbarComponent.prototype.ngOnInit = function () {
    };
    TopbarComponent.prototype.logout = function () {
        this.doLogout.emit();
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
        __metadata("design:type", String)
    ], TopbarComponent.prototype, "email", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Output"])(),
        __metadata("design:type", _angular_core__WEBPACK_IMPORTED_MODULE_0__["EventEmitter"])
    ], TopbarComponent.prototype, "doLogout", void 0);
    TopbarComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-topbar',
            template: __webpack_require__(/*! ./topbar.component.html */ "./src/app/listpage/topbar/topbar.component.html"),
            styles: [__webpack_require__(/*! ./topbar.component.css */ "./src/app/listpage/topbar/topbar.component.css")]
        }),
        __metadata("design:paramtypes", [])
    ], TopbarComponent);
    return TopbarComponent;
}());



/***/ }),

/***/ "./src/app/loginpage/loginpage.component.css":
/*!***************************************************!*\
  !*** ./src/app/loginpage/loginpage.component.css ***!
  \***************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".lowerit {\n  padding: 100px 0px;\n}\n\n\n"

/***/ }),

/***/ "./src/app/loginpage/loginpage.component.html":
/*!****************************************************!*\
  !*** ./src/app/loginpage/loginpage.component.html ***!
  \****************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"container\">\n  <div class=\"col-sm-5 col-sm-offset-3\">\n    <div class=\"lowerit\">\n      <div class=\"panel panel-default\">\n        <div class=\"panel-heading\">\n          <h1 *ngIf=\"isSignup; else login_content\" class=\"text-center\">Sign Up</h1>\n          <ng-template #login_content><h1 class=\"text-center\">Job Application Tracker</h1></ng-template>\n        </div>\n        <div class=\"panel-body\">\n\n            <div class=\"input-group\">\n              <span class=\"input-group-addon\"><i class=\"glyphicon glyphicon-user\"></i></span>\n              <input (keyup)='validate(email, password)' type=\"email\" class=\"form-control\" placeholder=\"Enter email\"\n                     #email>\n            </div>\n          <br>\n\n            <div class=\"input-group\">\n            <span class=\"input-group-addon\"><i class=\"glyphicon glyphicon-lock\"></i></span>\n            <input (keyup)='validate(email, password)' type=\"password\" class=\"form-control\" id=\"password\"\n                   placeholder=\"Password\" #password>\n            </div>\n          <br>\n          <div *ngIf=\"isSignup\" class=\"input-group\">\n            <span class=\"input-group-addon\" for=\"postcode\">Postcode</span>\n            <input (keyup)='validate(email, password)' type=\"text\" class=\"form-control\" id=\"postcode\"\n                   placeholder=\"Postcode\" [(ngModel)]=\"postcode\">\n\n          </div>\n          <br *ngIf=\"isSignup\">\n          <button [disabled]='isButtonDisabled' (click)=\"logUser(email.value,password.value)\" type=\"submit\"\n                  class=\"btn btn-primary\">{{buttonText}}\n          </button>\n          <a *ngIf=\"!isSignup\" class=\"pull-right\" (click)=\"changeToLogin()\">Register</a>\n          <a *ngIf=\"isSignup\" class=\"pull-right\" (click)=\"changeToLogin()\">Already registered?</a><br>\n          <p *ngIf=\"badPassword\" class=\"text-center\"><em>The username or password are incorrect</em></p>\n        </div>\n      </div>\n    </div>\n  </div>\n</div>\n"

/***/ }),

/***/ "./src/app/loginpage/loginpage.component.ts":
/*!**************************************************!*\
  !*** ./src/app/loginpage/loginpage.component.ts ***!
  \**************************************************/
/*! exports provided: LoginpageComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LoginpageComponent", function() { return LoginpageComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _user_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../user.service */ "./src/app/user.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var LoginpageComponent = /** @class */ (function () {
    function LoginpageComponent(userService, router) {
        this.userService = userService;
        this.router = router;
        this.isSignup = false;
        this.postcode = '';
        this.buttonText = '';
        this.badPassword = false;
    }
    LoginpageComponent.prototype.ngOnInit = function () {
        this.isButtonDisabled = false;
        this.setButton();
    };
    LoginpageComponent.prototype.logUser = function (email, password) {
        var _this = this;
        if (this.isSignup) {
            // Should create a new user
            this.userService.signup(email, password, this.postcode)
                .subscribe(function (response) {
                // Should save the token in the service.
                _this.userService.storeUser(response);
                // Should route to the list of jobapplications.
                _this.router.navigate(['/list']);
            }, function (error) {
                if (error.error.code === 11000) {
                    alert('The username already exist, click on \'already registered\' instead \n if that is you or choose another email');
                }
            });
        }
        else {
            // Should login the existent user
            this.userService.login(email, password)
                .subscribe(function (response) {
                _this.userService.storeUser(response);
                _this.router.navigate(['list']);
            }, function (error) {
                _this.badPassword = true;
            });
        }
    };
    LoginpageComponent.prototype.changeToLogin = function () {
        this.isSignup = !(this.isSignup);
        this.setButton();
    };
    LoginpageComponent.prototype.setButton = function () {
        if (this.isSignup) {
            this.isButtonDisabled = true;
            this.buttonText = 'Sign Up';
        }
        else {
            this.isButtonDisabled = false;
            this.buttonText = 'Log-in';
        }
    };
    LoginpageComponent.prototype.validate = function (email, password) {
        this.badPassword = false;
        var theEmail = email.value;
        var thePass = password.value;
        if (!(this.isSignup)) {
            this.isButtonDisabled = false;
            return;
        }
        if (this.isValidEmail(theEmail) && thePass.length > 6 && this.postcode.length > 4) {
            this.isButtonDisabled = false;
        }
        else {
            this.isButtonDisabled = true;
        }
    };
    LoginpageComponent.prototype.isValidEmail = function (someEmail) {
        return (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(someEmail));
    };
    LoginpageComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-loginpage',
            template: __webpack_require__(/*! ./loginpage.component.html */ "./src/app/loginpage/loginpage.component.html"),
            styles: [__webpack_require__(/*! ./loginpage.component.css */ "./src/app/loginpage/loginpage.component.css")]
        }),
        __metadata("design:paramtypes", [_user_service__WEBPACK_IMPORTED_MODULE_1__["UserService"], _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"]])
    ], LoginpageComponent);
    return LoginpageComponent;
}());



/***/ }),

/***/ "./src/app/page-not-found/page-not-found.component.css":
/*!*************************************************************!*\
  !*** ./src/app/page-not-found/page-not-found.component.css ***!
  \*************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/page-not-found/page-not-found.component.html":
/*!**************************************************************!*\
  !*** ./src/app/page-not-found/page-not-found.component.html ***!
  \**************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<p>\n  page-not-found works!\n</p>\n"

/***/ }),

/***/ "./src/app/page-not-found/page-not-found.component.ts":
/*!************************************************************!*\
  !*** ./src/app/page-not-found/page-not-found.component.ts ***!
  \************************************************************/
/*! exports provided: PageNotFoundComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PageNotFoundComponent", function() { return PageNotFoundComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var PageNotFoundComponent = /** @class */ (function () {
    function PageNotFoundComponent() {
    }
    PageNotFoundComponent.prototype.ngOnInit = function () {
    };
    PageNotFoundComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-page-not-found',
            template: __webpack_require__(/*! ./page-not-found.component.html */ "./src/app/page-not-found/page-not-found.component.html"),
            styles: [__webpack_require__(/*! ./page-not-found.component.css */ "./src/app/page-not-found/page-not-found.component.css")]
        }),
        __metadata("design:paramtypes", [])
    ], PageNotFoundComponent);
    return PageNotFoundComponent;
}());



/***/ }),

/***/ "./src/app/user.model.ts":
/*!*******************************!*\
  !*** ./src/app/user.model.ts ***!
  \*******************************/
/*! exports provided: User */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "User", function() { return User; });
var User = /** @class */ (function () {
    function User(email, id, token, postcode) {
        this.email = email;
        this.id = id;
        this.token = token;
        this.postcode = postcode;
    }
    return User;
}());



/***/ }),

/***/ "./src/app/user.service.ts":
/*!*********************************!*\
  !*** ./src/app/user.service.ts ***!
  \*********************************/
/*! exports provided: UserService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UserService", function() { return UserService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _user_model__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./user.model */ "./src/app/user.model.ts");
/* harmony import */ var _appsettings__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./appsettings */ "./src/app/appsettings.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var UserService = /** @class */ (function () {
    function UserService(http) {
        this.http = http;
        this.headerJson = {
            'Content-Type': 'application/json',
        };
    }
    UserService.prototype.signup = function (email, password, postcode) {
        //Notice that password cannot be less than some number of characters. s
        var userData = {
            'email': email,
            'password': password,
            'postcode': postcode
        };
        return this.http.post(_appsettings__WEBPACK_IMPORTED_MODULE_3__["AppSettings"].API_ENDPOINT + 'users', userData, { headers: this.headerJson, observe: 'response' });
    };
    UserService.prototype.login = function (email, password) {
        var userData = {
            'email': email,
            'password': password
        };
        return this.http.post(_appsettings__WEBPACK_IMPORTED_MODULE_3__["AppSettings"].API_ENDPOINT + 'users/login', userData, { headers: this.headerJson, observe: 'response' });
    };
    UserService.prototype.storeUser = function (response) {
        this.theUser = new _user_model__WEBPACK_IMPORTED_MODULE_2__["User"](response.body.email, response.body._id, response.headers.get('x-auth'), response.body.postcode);
        localStorage.setItem('User', JSON.stringify(this.theUser));
    };
    UserService.prototype.inserToken = function (aToken) {
        this.headerJson['x-auth'] = aToken;
    };
    UserService.prototype.getUser = function () {
        return this.theUser;
    };
    UserService.prototype.checkLoggedIn = function () {
        if (JSON.parse(localStorage.getItem('User')) === null) {
            return false;
        }
        else {
            this.logUser();
            return true;
        }
    };
    UserService.prototype.logUser = function () {
        this.theUser = JSON.parse(localStorage.getItem('User'));
        //Should also delete the token
    };
    UserService.prototype.logout = function () {
        localStorage.removeItem('User');
        this.theUser = null;
    };
    UserService = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])({
            providedIn: 'root'
        }),
        __metadata("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"]])
    ], UserService);
    return UserService;
}());



/***/ }),

/***/ "./src/environments/environment.ts":
/*!*****************************************!*\
  !*** ./src/environments/environment.ts ***!
  \*****************************************/
/*! exports provided: environment */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "environment", function() { return environment; });
// This file can be replaced during build by using the `fileReplacements` array.
// `ng build ---prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.
var environment = {
    production: false
};
/*
 * In development mode, to ignore zone related error stack frames such as
 * `zone.run`, `zoneDelegate.invokeTask` for easier debugging, you can
 * import the following file, but please comment it out in production mode
 * because it will have performance impact when throw error
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.


/***/ }),

/***/ "./src/main.ts":
/*!*********************!*\
  !*** ./src/main.ts ***!
  \*********************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/platform-browser-dynamic */ "./node_modules/@angular/platform-browser-dynamic/fesm5/platform-browser-dynamic.js");
/* harmony import */ var _app_app_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./app/app.module */ "./src/app/app.module.ts");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./environments/environment */ "./src/environments/environment.ts");




if (_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].production) {
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["enableProdMode"])();
}
Object(_angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_1__["platformBrowserDynamic"])().bootstrapModule(_app_app_module__WEBPACK_IMPORTED_MODULE_2__["AppModule"])
    .catch(function (err) { return console.log(err); });


/***/ }),

/***/ 0:
/*!***************************!*\
  !*** multi ./src/main.ts ***!
  \***************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! /mnt/c/Users/Maikel/Documents/project/frontendTM470/src/main.ts */"./src/main.ts");


/***/ })

},[[0,"runtime","vendor"]]]);
//# sourceMappingURL=main.js.map