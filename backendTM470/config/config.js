const mongoose = require('mongoose');
const serverconfig = require('./serverconfig.json');

//The trim method here solved a bug that stopped the comparison.
const env = (process.env.NODE_ENV || 'development').trim();

// If the environment is production it won't need to load this values
// because they would have been assigned in the server on production.
if (env === 'development' || env === 'test') {
  const envConfig = serverconfig[env];
  Object.keys(envConfig).forEach((key) => {
    process.env[key] = envConfig[key];
  });
}

mongoose.Promise = global.Promise;
mongoose.connect(process.env.MONGODB_URI, {
  useMongoClient: true
});

module.exports = {mongoose};
