var express = require('express');
var router = express.Router();
var {authenticate} = require('./../middleware/authenticate');
const _ = require('lodash');
var {User} = require('./../models/user');

/**
 * @api {post} /api/users Creates (posts) a new user
 * @apiVersion 1.0.0
 * @apiName PostUser
 * @apiGroup User
 *
 * @apiHeader {String} Content-Type Must be application/json and is required
* @apiParam {String} email The email of the user which must be non-existent on the API
* @apiParam {String} password A password which must have a minimum length of 6 characters

 *
 * @apiSuccess {String} _id the ID of the user
 * @apiSuccess {String} email the email of the user
 * @apiSuccess {String} x-auth  Header with the token of the user
 *
 * @apiSuccessExample Success-Response
 *     HTTP/1.1 200 OK
 *     {
 *       "_id": "5ac181f6d031c63248d3be2e",
 *       "email": "maikel@maikel.suk",
 *       "x-auth":  "eyJhbGciOiJIUzI1NiIsInR5cCI6..."
 *     }


 *
 * @apiError DuplicateKeyError The user already exists
 *
 * @apiErrorExample DuplicateKeyError
 {
     "code": 11000,
     "index": 0,
     "errmsg": "E11000 duplicate key error collection: 5aa69b219701996c83b3fa5d_jobappdb.users index: email_1 dup key: { : \"maikel@maikel.suk\" }",
     "op": {
         "email": "maikel@maikel.suk",
         "password": "$2a$04$un7YEKBpyvcU4IxwiG2fhuOXILYnOlgONcnwmJZkrKrwLqAUmO7vW",
         "_id": "5ac1827c0ba2f80a84371e1f",
         "tokens": [],
         "__v": 0
     }
 }

 * @apiError TooShortPassword THe password is less than six characters
 *
 * @apiErrorExample TooShortPassword
 {
    "errors": {
        "password": {
            "message": "Path `password` (`caaca`) is shorter than the minimum allowed length (6).",
            "name": "ValidatorError",
            "properties": {
                "minlength": 6,
                "type": "minlength",
                "message": "Path `{PATH}` (`{VALUE}`) is shorter than the minimum allowed length (6).",
                "path": "password",
                "value": "caaca"
            },
            "kind": "minlength",
            "path": "password",
            "value": "caaca",
            "$isValidatorError": true
        }
    },
    "_message": "User validation failed",
    "message": "User validation failed: password: Path `password` (`caaca`) is shorter than the minimum allowed length (6).",
    "name": "ValidationError"
}
 */
router.post('/users', (req, res) => {
  var body = req.body;
  var user = new User(body);
  user.save().then(() => {
    return user.generateAuthToken();
  }).then((token) => {
    res.header('x-auth', token).send(user);
  }).catch((e) => {
    res.status(400).send(e);
  })
});

/**
 * @api {get} /api/users/me Gets user ID having the token (authenticates)
* @apiVersion 1.0.0
 * @apiName GetUser
 * @apiGroup User
 *
 * @apiHeader {String} Content-Type Must be application/json and is required
 * @apiHeader {String} x-auth The valid token of that user

 * @apiSuccess {String} _id the ID of the user
 * @apiSuccess {String} email the email of the user
* @apiSuccess {String} x-auth  Header with the token of the user
 *
 * @apiSuccessExample Success-Response
 *     HTTP/1.1 200 OK
 *     {
 *       "_id": "5ac181f6d031c63248d3be2e",
 *       "email": "maikel@maikel.suk"
 *     }
 *
 * @apiError Unauthorised The user doesn't exist or the token is invalid or missing.
 * @apiErrorExample Error-Response
 *     HTTP/1.1 401 Unauthorised
 *
 */
router.get('/users/me', authenticate, (req, res) => {
  res.send(req.user);
});

/* This is that sending a user and password gets a token or generates a new one if the users
hasn't previously logged in */
/**
 * @api {post} /api/users/login Gets user token (logins) by giving the user and password
 * @apiVersion 1.0.0
 * @apiName Post
 * @apiGroup User
 *
 * @apiHeader {String} Content-Type Must be application/json and is required
 * @apiHeader {String} x-auth The valid token of that user
 * @apiParam {String} email The email of the user which must be non-existent on the API
 * @apiParam {String} password A password which must have a minimum length of 6 characters

 * @apiSuccess {String} _id the ID of the user
 * @apiSuccess {String} email the email of the user
* @apiSuccess {String} x-auth  Header with the token of the user
 *
 * @apiSuccessExample Success-Response
 *     HTTP/1.1 200 OK
 *     {
 *       "_id": "5ac181f6d031c63248d3be2e",
 *       "email": "maikel@maikel.suk",
 *       "x-auth":  "eyJhbGciOiJIUzI1NiIsInR5cCI6..."
 *     }
 *
 * @apiError Unauthorised The user doesn't exist or the token is invalid or missing.
 * @apiErrorExample Error-Response
 *     HTTP/1.1 401 Unauthorised
 *
 */
router.post('/users/login', (req, res) => {
  var body = _.pick(req.body, ['email', 'password']);
  User.findByCredentials(body.email, body.password).then((user) => {
    return user.generateAuthToken().then((token) => {
      res.header('x-auth', token).send(user);
    });
  }).catch((e) => {
    res.status(400).send();
  });
});

// To delete a token, equivalent to login out.
/**
 * @api {delete} /api/users/me/token To delete a token (login out).
* @apiVersion 1.0.0
 * @apiName DeleteToken
 * @apiGroup User
 *
 * @apiHeader {String} Content-Type Must be application/json and is required
 * @apiHeader {String} x-auth The valid token of that user to delete
 * @apiSuccessExample Success-Response
 *     HTTP/1.1 200 OK
 *
 * @apiError Unauthorised The user doesn't exist or the token is invalid or missing.
 * @apiErrorExample Error-Response
 *     HTTP/1.1 401 Unauthorised
 *
 */
router.delete('/users/me/token', authenticate, (req, res) => {
  req.user.removeToken(req.token).then(() => {
    res.status(200).send();
  }, () => {
    res.status(400).send();
  });
});

module.exports = router;
