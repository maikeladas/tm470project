var express = require('express');
var router = express.Router();
var {authenticate} = require('./../middleware/authenticate');
var {JobApplication} = require('./../models/jobapplication');
const {ObjectID} = require('mongodb');
const _ = require('lodash');
var request = require('request');

/**
* @apiDefine Unauthorised
* @apiError Unauthorised The user doesn't exist or the token is invalid or missing.
* @apiErrorExample Unauthorised
*     HTTP/1.1 401 Unauthorised
*
*/

/**
* @apiDefine NoFound
* @apiError NoFound The job application doesn´t exist
* @apiErrorExample NoFound
*     HTTP/1.1 404 Not Found
*
*/

/**
 * @api {post} /api/jobapplications Creates (posts) a new jobapplication
* @apiVersion 1.0.0
 * @apiName PostJobapp
 * @apiGroup JobApplication
 *
 * @apiHeader {String} Content-Type Must be application/json (required)
 * @apiHeader {String} x-auth The token of the user (required)
* @apiParam {String} notes  The content of the note itself.
* @apiParam {String} roleName The role name of that job (required)
* @apiParam {String} dateApplied The date the user applied for it
* @apiParam {String} companyName The name of the company
* @apiParam {String} location The location of this job
* @apiParam {String} referenceLetterName The name of the reference letter used
* @apiParam {String} cvUsedName The name of the CV used.

 * @apiSuccess {Number} _v Mongoose control number
 * @apiSuccess {String} notes  The content of the note itself.
 * @apiSuccess {String} roleName The role name of that job (required)
 * @apiSuccess {String} dateApplied The date the user applied for it
 * @apiSuccess {String} companyName The name of the company
 * @apiSuccess {String} location The location of this job
 * @apiSuccess {String} referenceLetterName The name of the reference letter used
 * @apiSuccess {String} cvUsedName The name of the CV used.
 * @apiSuccess {String} _creator ID of the user who created it
 * @apiSuccess {String} _id The ID of the new note *
 * @apiSuccessExample Success-Response
 *     HTTP/1.1 200 OK
 {
     "__v": 0,
     "notes": "Hello I am a note",
     "roleName": "Waiter",
     "_creator": "5ac18dd454dfdc699cdf6933",
     "_id": "5ac18de154dfdc699cdf6935"
 }

 *
 * @apiError ValidationError roleName was empty
 *
 * @apiErrorExample ValidationError
 {
    "errors": {
        "roleName": {
            "message": "Path `roleName` is required.",
            "name": "ValidatorError",
            "properties": {
                "type": "required",
                "message": "Path `{PATH}` is required.",
                "path": "roleName"
            },
            "kind": "required",
            "path": "roleName",
            "$isValidatorError": true
        }
    },
    "_message": "JobApplication validation failed",
    "message": "JobApplication validation failed: roleName: Path `roleName` is required.",


* @apiUse Unauthorised
    */
router.post('/jobapplications', authenticate, (req, res) => {
    var body = req.body;
    body['_creator'] = req.user._id;
    var distance = 0;

request.get('https://maps.googleapis.com/maps/api/distancematrix/json?' +
    'units=metric&' +
    'origins=' + body.location +
    '&destinations=' + req.user.postcode + '&key=AIzaSyCvk_e8MCvFdvy4Xi8l1jYsWXiedbefNUk',
    function(err, res2, content) {
        // The value of distance in metres
        console.log(content);
        if (JSON.parse(content).rows[0].elements[0].status === 'OK') {
            console.log('found');
            distance = JSON.parse(content).rows[0].elements[0].distance.value;
        } else {
            console.log('not found');
            distance = -1;
        }
        body['distance'] = distance;
        var jobapplication = new JobApplication(body);
        jobapplication.save().then((doc) => {
            res.send(doc);
    }, (e) => {
            res.status(400).send(e);
        });
    });
});



/**
 * @api {get} /api/jobapplications Gets all the jobs application by a given user
 * @apiName GetJobApps
 * @apiVersion 1.0.0
 * @apiGroup JobApplication

  * @apiHeader {String} Content-Type Must be application/json (required)
  * @apiHeader {String} x-auth The token of the user (required)

  * @apiSuccess {Object} jobapplications Object containing all job applications

  * @apiSuccessExample Success-Response
  *     HTTP/1.1 200 OK
  {
      "jobapplications": [
          {
              "_id": "5ac20312f44e1c15c8c7602f",
              "notes": "Hello I am a note",
              "roleName": "Waiter",
              "_creator": "5ab6f6e641358a1fe09f025d",
              "__v": 0
          }
      ]
  }

* @apiUse Unauthorised
*/
router.get('/jobapplications', authenticate, (req, res) => {
  JobApplication.find({
    _creator: req.user._id
  }).then((jobapplications) => {
    res.send({jobapplications});
  }, (e) => {
    res.status(400).send(e);
  });
});

/**
 * @api {get} /api/jobapplications/:id Gets a job application by ID and user.
 * @apiName GetJobApp
 * @apiVersion 1.0.0
 * @apiGroup JobApplication

   * @apiHeader {String} Content-Type Must be application/json (required)
   * @apiHeader {String} x-auth The token of the user (required)
  * @apiParam {String} _id The ID of the application (required)

  * @apiSuccess {Number} _v Mongoose control number
  * @apiSuccess {String} notes  The content of the note itself.
  * @apiSuccess {String} roleName The role name of that job (required)
  * @apiSuccess {String} dateApplied The date the user applied for it
  * @apiSuccess {String} companyName The name of the company
  * @apiSuccess {String} location The location of this job
  * @apiSuccess {String} referenceLetterName The name of the reference letter used
  * @apiSuccess {String} cvUsedName The name of the CV used.
  * @apiSuccess {String} _creator ID of the user who created it
  * @apiSuccess {String} _id The ID of the new note *
  * @apiSuccessExample Success-Response
  *     HTTP/1.1 200 OK
  {
      "__v": 0,
      "notes": "Hello I am a note",
      "roleName": "Waiter",
      "_creator": "5ac18dd454dfdc699cdf6933",
      "_id": "5ac18de154dfdc699cdf6935"
  }
* @apiUse NoFound
 * @apiUse Unauthorised
 */
router.get('/jobapplications/:id', authenticate, (req, res) => {
  var id = req.params.id;

  if (!ObjectID.isValid(id)) {
    return res.status(404).send();
  }

  JobApplication.findOne({
    _id: id,
    _creator: req.user._id
  }).then((jobapplication) => {
    if (!jobapplication) {
      return res.status(404).send();
    }

    res.send({jobapplication});
  }).catch((e) => {
    res.status(400).send();
  });
});

//Deletes a job application by ID and user token
/**
 * @api {delete} /api/jobapplications/:id Deletes a job application by ID and user.
 * @apiName DeleteJobApp
 * @apiVersion 1.0.0
 * @apiGroup JobApplication

    * @apiHeader {String} Content-Type Must be application/json (required)
    * @apiHeader {String} x-auth The token of the user (required)
   * @apiParam {String} _id The ID of the application (required)

   * @apiSuccess {Number} _v Mongoose control number
   * @apiSuccess {String} notes  The content of the note itself.
   * @apiSuccess {String} roleName The role name of that job (required)
   * @apiSuccess {String} dateApplied The date the user applied for it
   * @apiSuccess {String} companyName The name of the company
   * @apiSuccess {String} location The location of this job
   * @apiSuccess {String} referenceLetterName The name of the reference letter used
   * @apiSuccess {String} cvUsedName The name of the CV used.
   * @apiSuccess {String} _creator ID of the user who created it
   * @apiSuccess {String} _id The ID of the new note *
   * @apiSuccessExample Success-Response
   *     HTTP/1.1 200 OK
   {
       "__v": 0,
       "notes": "Hello I am a note",
       "roleName": "Waiter",
       "_creator": "5ac18dd454dfdc699cdf6933",
       "_id": "5ac18de154dfdc699cdf6935"
   }
 * @apiUse NoFound
  * @apiUse Unauthorised
  */
router.delete('/jobapplications/:id', authenticate, (req, res) => {
  var id = req.params.id;

  if (!ObjectID.isValid(id)) {
    return res.status(404).send();
  }

  JobApplication.findOneAndRemove({
    _id: id,
    _creator: req.user._id
  }).then((jobapplication) => {
    if (!jobapplication) {
      return res.status(404).send();
    }

    res.send({jobapplication});
  }).catch((e) => {
    res.status(400).send();
  });
});

/**
 * @api {patch} /api/jobapplications/:id Modifies a job application by ID and user.
 * @apiName ModJobApp
 * @apiVersion 1.0.0
 * @apiGroup JobApplication

    * @apiHeader {String} Content-Type Must be application/json (required)
    * @apiHeader {String} x-auth The token of the user (required)
   * @apiParam {String} _id The ID of the application (required)

   * @apiSuccess {Number} _v Mongoose control number
   * @apiSuccess {String} notes  The content of the note itself.
   * @apiSuccess {String} roleName The role name of that job (required)
   * @apiSuccess {String} dateApplied The date the user applied for it
   * @apiSuccess {String} companyName The name of the company
   * @apiSuccess {String} location The location of this job
   * @apiSuccess {String} referenceLetterName The name of the reference letter used
   * @apiSuccess {String} cvUsedName The name of the CV used.
   * @apiSuccess {String} _creator ID of the user who created it
   * @apiSuccess {String} _id The ID of the new note *
   * @apiSuccessExample Success-Response
   *     HTTP/1.1 200 OK
   {
       "__v": 0,
       "notes": "Hello I am a note",
       "roleName": "Waiter",
       "_creator": "5ac18dd454dfdc699cdf6933",
       "_id": "5ac18de154dfdc699cdf6935"
   }
 * @apiUse NoFound
  * @apiUse Unauthorised
  */
router.patch('/jobapplications/:id', authenticate, (req, res) => {
  var id = req.params.id;
  var body = req.body;
  var distance = 0;
if (!ObjectID.isValid(id)) {
    return res.status(404).send();
}


  request.get('https://maps.googleapis.com/maps/api/distancematrix/json?' +
      'units=metric&' +
      'origins=' + body.location +
      '&destinations=' + req.user.postcode + '&key=AIzaSyCvk_e8MCvFdvy4Xi8l1jYsWXiedbefNUk',
      function(err, res2, content) {
          // The value of distance in metres
          if (JSON.parse(content).rows[0].elements[0].status === 'OK') {
              console.log('found');
              distance = JSON.parse(content).rows[0].elements[0].distance.value;
          } else {
              console.log('not found');
              distance = -1;
          }
          body['distance'] = distance;
          JobApplication.findOneAndUpdate({_id: id, _creator: req.user._id}, {$set: body}, {new: true}).then((jobapplication) => {
              if (!jobapplication) {
              return res.status(404).send();
          }
          res.send(jobapplication);
      }).catch((e) => {
              res.status(400).send();
      });

      });
});


module.exports = router;
