define({
  "name": "TM470 Backend Documentation",
  "version": "1.0.0",
  "description": "Documentation of each one of the Rest calls to this Job Application project backend",
  "title": "TM470 Backend Documentation",
  "sampleUrl": false,
  "defaultVersion": "0.0.0",
  "apidoc": "0.3.0",
  "generator": {
    "name": "apidoc",
    "time": "2018-06-25T21:02:04.373Z",
    "url": "http://apidocjs.com",
    "version": "0.17.6"
  }
});
