define({ "api": [
  {
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "optional": false,
            "field": "varname1",
            "description": "<p>No type.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "varname2",
            "description": "<p>With type.</p>"
          }
        ]
      }
    },
    "type": "",
    "url": "",
    "version": "0.0.0",
    "filename": "./doc/main.js",
    "group": "C__Users_cex_OneDrive_DisposableExperiments_project_backend_routes_doc_main_js",
    "groupTitle": "C__Users_cex_OneDrive_DisposableExperiments_project_backend_routes_doc_main_js",
    "name": ""
  },
  {
    "type": "delete",
    "url": "/api/jobapplications/:id",
    "title": "Deletes a job application by ID and user.",
    "name": "DeleteJobApp",
    "version": "1.0.0",
    "group": "JobApplication",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "Content-Type",
            "description": "<p>Must be application/json (required)</p>"
          },
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "x-auth",
            "description": "<p>The token of the user (required)</p>"
          }
        ]
      }
    },
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "_id",
            "description": "<p>The ID of the application (required)</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "_v",
            "description": "<p>Mongoose control number</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "notes",
            "description": "<p>The content of the note itself.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "roleName",
            "description": "<p>The role name of that job (required)</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "dateApplied",
            "description": "<p>The date the user applied for it</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "companyName",
            "description": "<p>The name of the company</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "location",
            "description": "<p>The location of this job</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "referenceLetterName",
            "description": "<p>The name of the reference letter used</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "cvUsedName",
            "description": "<p>The name of the CV used.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "_creator",
            "description": "<p>ID of the user who created it</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "_id",
            "description": "<p>The ID of the new note *</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response",
          "content": " HTTP/1.1 200 OK\n{\n    \"__v\": 0,\n    \"notes\": \"Hello I am a note\",\n    \"roleName\": \"Waiter\",\n    \"_creator\": \"5ac18dd454dfdc699cdf6933\",\n    \"_id\": \"5ac18de154dfdc699cdf6935\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "./jobapplications.js",
    "groupTitle": "JobApplication",
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "NoFound",
            "description": "<p>The job application doesn´t exist</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "Unauthorised",
            "description": "<p>The user doesn't exist or the token is invalid or missing.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "NoFound",
          "content": "HTTP/1.1 404 Not Found",
          "type": "json"
        },
        {
          "title": "Unauthorised",
          "content": "HTTP/1.1 401 Unauthorised",
          "type": "json"
        }
      ]
    }
  },
  {
    "type": "get",
    "url": "/api/jobapplications/:id",
    "title": "Gets a job application by ID and user.",
    "name": "GetJobApp",
    "version": "1.0.0",
    "group": "JobApplication",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "Content-Type",
            "description": "<p>Must be application/json (required)</p>"
          },
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "x-auth",
            "description": "<p>The token of the user (required)</p>"
          }
        ]
      }
    },
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "_id",
            "description": "<p>The ID of the application (required)</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "_v",
            "description": "<p>Mongoose control number</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "notes",
            "description": "<p>The content of the note itself.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "roleName",
            "description": "<p>The role name of that job (required)</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "dateApplied",
            "description": "<p>The date the user applied for it</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "companyName",
            "description": "<p>The name of the company</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "location",
            "description": "<p>The location of this job</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "referenceLetterName",
            "description": "<p>The name of the reference letter used</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "cvUsedName",
            "description": "<p>The name of the CV used.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "_creator",
            "description": "<p>ID of the user who created it</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "_id",
            "description": "<p>The ID of the new note *</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response",
          "content": "  HTTP/1.1 200 OK\n{\n    \"__v\": 0,\n    \"notes\": \"Hello I am a note\",\n    \"roleName\": \"Waiter\",\n    \"_creator\": \"5ac18dd454dfdc699cdf6933\",\n    \"_id\": \"5ac18de154dfdc699cdf6935\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "./jobapplications.js",
    "groupTitle": "JobApplication",
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "NoFound",
            "description": "<p>The job application doesn´t exist</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "Unauthorised",
            "description": "<p>The user doesn't exist or the token is invalid or missing.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "NoFound",
          "content": "HTTP/1.1 404 Not Found",
          "type": "json"
        },
        {
          "title": "Unauthorised",
          "content": "HTTP/1.1 401 Unauthorised",
          "type": "json"
        }
      ]
    }
  },
  {
    "type": "get",
    "url": "/api/jobapplications",
    "title": "Gets all the jobs application by a given user",
    "name": "GetJobApps",
    "version": "1.0.0",
    "group": "JobApplication",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "Content-Type",
            "description": "<p>Must be application/json (required)</p>"
          },
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "x-auth",
            "description": "<p>The token of the user (required)</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "jobapplications",
            "description": "<p>Object containing all job applications</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response",
          "content": "  HTTP/1.1 200 OK\n{\n    \"jobapplications\": [\n        {\n            \"_id\": \"5ac20312f44e1c15c8c7602f\",\n            \"notes\": \"Hello I am a note\",\n            \"roleName\": \"Waiter\",\n            \"_creator\": \"5ab6f6e641358a1fe09f025d\",\n            \"__v\": 0\n        }\n    ]\n}",
          "type": "json"
        }
      ]
    },
    "filename": "./jobapplications.js",
    "groupTitle": "JobApplication",
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "Unauthorised",
            "description": "<p>The user doesn't exist or the token is invalid or missing.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Unauthorised",
          "content": "HTTP/1.1 401 Unauthorised",
          "type": "json"
        }
      ]
    }
  },
  {
    "type": "patch",
    "url": "/api/jobapplications/:id",
    "title": "Modifies a job application by ID and user.",
    "name": "ModJobApp",
    "version": "1.0.0",
    "group": "JobApplication",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "Content-Type",
            "description": "<p>Must be application/json (required)</p>"
          },
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "x-auth",
            "description": "<p>The token of the user (required)</p>"
          }
        ]
      }
    },
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "_id",
            "description": "<p>The ID of the application (required)</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "_v",
            "description": "<p>Mongoose control number</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "notes",
            "description": "<p>The content of the note itself.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "roleName",
            "description": "<p>The role name of that job (required)</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "dateApplied",
            "description": "<p>The date the user applied for it</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "companyName",
            "description": "<p>The name of the company</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "location",
            "description": "<p>The location of this job</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "referenceLetterName",
            "description": "<p>The name of the reference letter used</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "cvUsedName",
            "description": "<p>The name of the CV used.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "_creator",
            "description": "<p>ID of the user who created it</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "_id",
            "description": "<p>The ID of the new note *</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response",
          "content": " HTTP/1.1 200 OK\n{\n    \"__v\": 0,\n    \"notes\": \"Hello I am a note\",\n    \"roleName\": \"Waiter\",\n    \"_creator\": \"5ac18dd454dfdc699cdf6933\",\n    \"_id\": \"5ac18de154dfdc699cdf6935\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "./jobapplications.js",
    "groupTitle": "JobApplication",
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "NoFound",
            "description": "<p>The job application doesn´t exist</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "Unauthorised",
            "description": "<p>The user doesn't exist or the token is invalid or missing.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "NoFound",
          "content": "HTTP/1.1 404 Not Found",
          "type": "json"
        },
        {
          "title": "Unauthorised",
          "content": "HTTP/1.1 401 Unauthorised",
          "type": "json"
        }
      ]
    }
  },
  {
    "type": "post",
    "url": "/api/jobapplications",
    "title": "Creates (posts) a new jobapplication",
    "version": "1.0.0",
    "name": "PostJobapp",
    "group": "JobApplication",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "Content-Type",
            "description": "<p>Must be application/json (required)</p>"
          },
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "x-auth",
            "description": "<p>The token of the user (required)</p>"
          }
        ]
      }
    },
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "notes",
            "description": "<p>The content of the note itself.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "roleName",
            "description": "<p>The role name of that job (required)</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "dateApplied",
            "description": "<p>The date the user applied for it</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "companyName",
            "description": "<p>The name of the company</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "location",
            "description": "<p>The location of this job</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "referenceLetterName",
            "description": "<p>The name of the reference letter used</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "cvUsedName",
            "description": "<p>The name of the CV used.</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "_v",
            "description": "<p>Mongoose control number</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "notes",
            "description": "<p>The content of the note itself.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "roleName",
            "description": "<p>The role name of that job (required)</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "dateApplied",
            "description": "<p>The date the user applied for it</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "companyName",
            "description": "<p>The name of the company</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "location",
            "description": "<p>The location of this job</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "referenceLetterName",
            "description": "<p>The name of the reference letter used</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "cvUsedName",
            "description": "<p>The name of the CV used.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "_creator",
            "description": "<p>ID of the user who created it</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "_id",
            "description": "<p>The ID of the new note *</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response",
          "content": "   HTTP/1.1 200 OK\n{\n    \"__v\": 0,\n    \"notes\": \"Hello I am a note\",\n    \"roleName\": \"Waiter\",\n    \"_creator\": \"5ac18dd454dfdc699cdf6933\",\n    \"_id\": \"5ac18de154dfdc699cdf6935\"\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "ValidationError",
            "description": "<p>roleName was empty</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "Unauthorised",
            "description": "<p>The user doesn't exist or the token is invalid or missing.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "ValidationError",
          "content": "{\n   \"errors\": {\n       \"roleName\": {\n           \"message\": \"Path `roleName` is required.\",\n           \"name\": \"ValidatorError\",\n           \"properties\": {\n               \"type\": \"required\",\n               \"message\": \"Path `{PATH}` is required.\",\n               \"path\": \"roleName\"\n           },\n           \"kind\": \"required\",\n           \"path\": \"roleName\",\n           \"$isValidatorError\": true\n       }\n   },\n   \"_message\": \"JobApplication validation failed\",\n   \"message\": \"JobApplication validation failed: roleName: Path `roleName` is required.\",",
          "type": "json"
        },
        {
          "title": "Unauthorised",
          "content": "HTTP/1.1 401 Unauthorised",
          "type": "json"
        }
      ]
    },
    "filename": "./jobapplications.js",
    "groupTitle": "JobApplication"
  },
  {
    "type": "delete",
    "url": "/api/users/me/token",
    "title": "To delete a token (login out).",
    "version": "1.0.0",
    "name": "DeleteToken",
    "group": "User",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "Content-Type",
            "description": "<p>Must be application/json and is required</p>"
          },
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "x-auth",
            "description": "<p>The valid token of that user to delete</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response",
          "content": "HTTP/1.1 200 OK",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "Unauthorised",
            "description": "<p>The user doesn't exist or the token is invalid or missing.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response",
          "content": "HTTP/1.1 401 Unauthorised",
          "type": "json"
        }
      ]
    },
    "filename": "./users.js",
    "groupTitle": "User"
  },
  {
    "type": "get",
    "url": "/api/users/me",
    "title": "Gets user ID having the token (authenticates)",
    "version": "1.0.0",
    "name": "GetUser",
    "group": "User",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "Content-Type",
            "description": "<p>Must be application/json and is required</p>"
          },
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "x-auth",
            "description": "<p>The valid token of that user</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "_id",
            "description": "<p>the ID of the user</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "email",
            "description": "<p>the email of the user</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "x-auth",
            "description": "<p>Header with the token of the user</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response",
          "content": "HTTP/1.1 200 OK\n{\n  \"_id\": \"5ac181f6d031c63248d3be2e\",\n  \"email\": \"maikel@maikel.suk\"\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "Unauthorised",
            "description": "<p>The user doesn't exist or the token is invalid or missing.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response",
          "content": "HTTP/1.1 401 Unauthorised",
          "type": "json"
        }
      ]
    },
    "filename": "./users.js",
    "groupTitle": "User"
  },
  {
    "type": "post",
    "url": "/api/users/login",
    "title": "Gets user token (logins) by giving the user and password",
    "version": "1.0.0",
    "name": "Post",
    "group": "User",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "Content-Type",
            "description": "<p>Must be application/json and is required</p>"
          },
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "x-auth",
            "description": "<p>The valid token of that user</p>"
          }
        ]
      }
    },
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "email",
            "description": "<p>The email of the user which must be non-existent on the API</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "password",
            "description": "<p>A password which must have a minimum length of 6 characters</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "_id",
            "description": "<p>the ID of the user</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "email",
            "description": "<p>the email of the user</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "x-auth",
            "description": "<p>Header with the token of the user</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response",
          "content": "HTTP/1.1 200 OK\n{\n  \"_id\": \"5ac181f6d031c63248d3be2e\",\n  \"email\": \"maikel@maikel.suk\",\n  \"x-auth\":  \"eyJhbGciOiJIUzI1NiIsInR5cCI6...\"\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "Unauthorised",
            "description": "<p>The user doesn't exist or the token is invalid or missing.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response",
          "content": "HTTP/1.1 401 Unauthorised",
          "type": "json"
        }
      ]
    },
    "filename": "./users.js",
    "groupTitle": "User"
  },
  {
    "type": "post",
    "url": "/api/users",
    "title": "Creates (posts) a new user",
    "version": "1.0.0",
    "name": "PostUser",
    "group": "User",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "Content-Type",
            "description": "<p>Must be application/json and is required</p>"
          }
        ]
      }
    },
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "email",
            "description": "<p>The email of the user which must be non-existent on the API</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "password",
            "description": "<p>A password which must have a minimum length of 6 characters</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "_id",
            "description": "<p>the ID of the user</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "email",
            "description": "<p>the email of the user</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "x-auth",
            "description": "<p>Header with the token of the user</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response",
          "content": "HTTP/1.1 200 OK\n{\n  \"_id\": \"5ac181f6d031c63248d3be2e\",\n  \"email\": \"maikel@maikel.suk\",\n  \"x-auth\":  \"eyJhbGciOiJIUzI1NiIsInR5cCI6...\"\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "DuplicateKeyError",
            "description": "<p>The user already exists</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "TooShortPassword",
            "description": "<p>THe password is less than six characters</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "DuplicateKeyError",
          "content": "{\n    \"code\": 11000,\n    \"index\": 0,\n    \"errmsg\": \"E11000 duplicate key error collection: 5aa69b219701996c83b3fa5d_jobappdb.users index: email_1 dup key: { : \\\"maikel@maikel.suk\\\" }\",\n    \"op\": {\n        \"email\": \"maikel@maikel.suk\",\n        \"password\": \"$2a$04$un7YEKBpyvcU4IxwiG2fhuOXILYnOlgONcnwmJZkrKrwLqAUmO7vW\",\n        \"_id\": \"5ac1827c0ba2f80a84371e1f\",\n        \"tokens\": [],\n        \"__v\": 0\n    }\n}",
          "type": "json"
        },
        {
          "title": "TooShortPassword",
          "content": " {\n    \"errors\": {\n        \"password\": {\n            \"message\": \"Path `password` (`caaca`) is shorter than the minimum allowed length (6).\",\n            \"name\": \"ValidatorError\",\n            \"properties\": {\n                \"minlength\": 6,\n                \"type\": \"minlength\",\n                \"message\": \"Path `{PATH}` (`{VALUE}`) is shorter than the minimum allowed length (6).\",\n                \"path\": \"password\",\n                \"value\": \"caaca\"\n            },\n            \"kind\": \"minlength\",\n            \"path\": \"password\",\n            \"value\": \"caaca\",\n            \"$isValidatorError\": true\n        }\n    },\n    \"_message\": \"User validation failed\",\n    \"message\": \"User validation failed: password: Path `password` (`caaca`) is shorter than the minimum allowed length (6).\",\n    \"name\": \"ValidationError\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "./users.js",
    "groupTitle": "User"
  }
] });
