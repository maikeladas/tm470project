const {ObjectID} = require('mongodb');
const jwt = require('jsonwebtoken');

const {JobApplication} = require('./../models/jobapplication');
const {User} = require('./../models/user');

const userOneId = new ObjectID();
const userTwoId = new ObjectID();
const users = [{
  _id: userOneId,
  email: 'maikel@maikel.uk',
  password: 'cacaca',
  tokens: [{
    access: 'auth',
    token: jwt.sign({_id: userOneId, access: 'auth'}, process.env.JWT_SECRET).toString()
  }]
}, {
  _id: userTwoId,
  email: 'jen@example.com',
  password: 'userTwoPass',
  tokens: [{
    access: 'auth',
    token: jwt.sign({_id: userTwoId, access: 'auth'}, process.env.JWT_SECRET).toString()
  }]
}];

const jobapplications = [{
  _id: new ObjectID(),
  notes: 'First test jobapplication',
  roleName: 'Waiter',
  _creator: userOneId
}, {
  _id: new ObjectID(),
  note: 'Second test jobapplication',
  roleName: 'Something else',
  _creator: userTwoId
}];

const populateJobApplications = (done) => {
  JobApplication.remove({}).then(() => {
    return JobApplication.insertMany(jobapplications);
  }).then(() => done());
};

const populateUsers = (done) => {
  User.remove({}).then(() => {
    var userOne = new User(users[0]).save();
    var userTwo = new User(users[1]).save();

    return Promise.all([userOne, userTwo])
  }).then(() => done());
};

module.exports = {jobapplications, populateJobApplications, users, populateUsers};
