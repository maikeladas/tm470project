const expect = require('expect');
const request = require('supertest');
const {ObjectID} = require('mongodb');

const {app} = require('./../index.js');
const {JobApplication} = require('./../models/jobapplication');
const {User} = require('./../models/user');

const {jobapplications, populateJobApplications, users, populateUsers} = require('./fakedata');

beforeEach(populateUsers);
beforeEach(populateJobApplications);

describe('POST /api/jobapplications', () => {
  it('should create a new jobapplication', (done) => {
    const jobapp = {
	     "notes": "Hello I am a note",
       "roleName": "Waiter"
     };

    request(app)
      .post('/api/jobapplications')
      .set('x-auth', users[0].tokens[0].token)
      .send(jobapp)
      .expect(200)
      .expect((res) => {
        expect(res.body.notes).toBe(jobapp.notes);
      })
      .end((err, res) => {
        if (err) {
          return done(err);
        }
        const notes = jobapp.notes;
        JobApplication.find({notes}).then((jobapplications) => {
          expect(jobapplications.length).toBe(1);
          expect(jobapplications[0].notes).toBe(notes);
          done();
        }).catch((e) => done(e));
      });
  });

  it('should not create jobapplication with invalid body data', (done) => {
    request(app)
      .post('/api/jobapplications')
      .set('x-auth', users[0].tokens[0].token)
      .send({})
      .expect(400)
      .end((err, res) => {
        if (err) {
          return done(err);
        }

        JobApplication.find().then((jobapplications) => {
          expect(jobapplications.length).toBe(2);
          done();
        }).catch((e) => done(e));
      });
  });
});

describe('GET /api/jobapplications', () => {
  it('should get all jobapplications', (done) => {
    request(app)
      .get('/api/jobapplications')
      .set('x-auth', users[0].tokens[0].token)
      .expect(200)
      .expect((res) => {
        expect(res.body.jobapplications.length).toBe(1);
      })
      .end(done);
  });
});

describe('GET /api/jobapplications/:id', () => {
  it('should return jobapplication doc', (done) => {
    request(app)
      .get(`/api/jobapplications/${jobapplications[0]._id.toHexString()}`)
      .set('x-auth', users[0].tokens[0].token)
      .expect(200)
      .expect((res) => {
        expect(res.body.jobapplication.text).toBe(jobapplications[0].text);
      })
      .end(done);
  });

  it('should not return jobapplication doc created by other user', (done) => {
    request(app)
      .get(`/api/api/jobapplications/${jobapplications[1]._id.toHexString()}`)
      .set('x-auth', users[0].tokens[0].token)
      .expect(404)
      .end(done);
  });

  it('should return 404 if jobapplication not found', (done) => {
    var hexId = new ObjectID().toHexString();

    request(app)
      .get(`/api/api/jobapplications/${hexId}`)
      .set('x-auth', users[0].tokens[0].token)
      .expect(404)
      .end(done);
  });

  it('should return 404 for non-object ids', (done) => {
    request(app)
      .get('/api/api/jobapplications/123abc')
      .set('x-auth', users[0].tokens[0].token)
      .expect(404)
      .end(done);
  });
});

describe('DELETE /api/jobapplications/:id', () => {
  it('should remove a jobapplication', (done) => {
    var hexId = jobapplications[1]._id.toHexString();

    request(app)
      .delete(`/api/jobapplications/${hexId}`)
      .set('x-auth', users[1].tokens[0].token)
      .expect(200)
      .expect((res) => {
        expect(res.body.jobapplication._id).toBe(hexId);
      })
      .end((err, res) => {
        if (err) {
          return done(err);
        }

        JobApplication.findById(hexId).then((jobapplication) => {
          expect(jobapplication).toNotExist();
          done();
        }).catch((e) => done(e));
      });
  });

  it('should remove a jobapplication', (done) => {
    var hexId = jobapplications[0]._id.toHexString();

    request(app)
      .delete(`/api/jobapplications/${hexId}`)
      .set('x-auth', users[1].tokens[0].token)
      .expect(404)
      .end((err, res) => {
        if (err) {
          return done(err);
        }

        JobApplication.findById(hexId).then((jobapplication) => {
          expect(jobapplication).toExist();
          done();
        }).catch((e) => done(e));
      });
  });

  it('should return 404 if jobapplication not found', (done) => {
    var hexId = new ObjectID().toHexString();

    request(app)
      .delete(`/api/jobapplications/${hexId}`)
      .set('x-auth', users[1].tokens[0].token)
      .expect(404)
      .end(done);
  });

  it('should return 404 if object id is invalid', (done) => {
    request(app)
      .delete('/api/jobapplications/123abc')
      .set('x-auth', users[1].tokens[0].token)
      .expect(404)
      .end(done);
  });
});

describe('PATCH /api/jobapplications/:id', () => {
  it('should update the jobapplication', (done) => {
    var hexId = jobapplications[0]._id.toHexString();
    var notes = 'This should be the new text';

    request(app)
      .patch(`/api/jobapplications/${hexId}`)
      .set('x-auth', users[0].tokens[0].token)
      .send({
        "whatever": "discard me",
        "notes" : notes
      })
      .expect(200)
      .expect((res) => {
        expect(res.body.notes).toBe(notes);
        expect(res.body.whatever).toNotExist;
      })
      .end(done);
  });

  it('should not update the jobapplication created by other user', (done) => {
    var hexId = jobapplications[0]._id.toHexString();
    var text = 'This should be the new text';

    request(app)
      .patch(`/api/jobapplications/${hexId}`)
      .set('x-auth', users[1].tokens[0].token)
      .send({
        completed: true,
        text
      })
      .expect(404)
      .end(done);
  });


});

describe('GET /api/users/me', () => {
  it('should return user if authenticated', (done) => {
    request(app)
      .get('/api/users/me')
      .set('x-auth', users[0].tokens[0].token)
      .expect(200)
      .expect((res) => {
        expect(res.body._id).toBe(users[0]._id.toHexString());
        expect(res.body.email).toBe(users[0].email);
      })
      .end(done);
  });

  it('should return 401 if not authenticated', (done) => {
    request(app)
      .get('/api/users/me')
      .expect(401)
      .expect((res) => {
        expect(res.body).toEqual({});
      })
      .end(done);
  });
});

describe('POST /api/users', () => {
  it('should create a user', (done) => {
    var email = 'example@example.com';
    var password = '123mnb!';

    request(app)
      .post('/api/users')
      .send({email, password})
      .expect(200)
      .expect((res) => {
        expect(res.headers['x-auth']).toExist();
        expect(res.body._id).toExist();
        expect(res.body.email).toBe(email);
      })
      .end((err) => {
        if (err) {
          return done(err);
        }

        User.findOne({email}).then((user) => {
          expect(user).toExist();
          expect(user.password).toNotBe(password);
          done();
        }).catch((e) => done(e));
      });
  });

  it('should return validation errors if request invalid', (done) => {
    request(app)
      .post('/api/users')
      .send({
        email: 'and',
        password: '123'
      })
      .expect(400)
      .end(done);
  });

  it('should not create user if email in use', (done) => {
    request(app)
      .post('/api/users')
      .send({
        email: users[0].email,
        password: 'Password123!'
      })
      .expect(400)
      .end(done);
  });
});

describe('POST /api/users/login', () => {
  it('should login user and return auth token', (done) => {
    request(app)
      .post('/api/users/login')
      .send({
        email: users[1].email,
        password: users[1].password
      })
      .expect(200)
      .expect((res) => {
        expect(res.headers['x-auth']).toExist();
      })
      .end((err, res) => {
        if (err) {
          return done(err);
        }

        User.findById(users[1]._id).then((user) => {
          expect(user.tokens[1]).toInclude({
            access: 'auth',
            token: res.headers['x-auth']
          });
          done();
        }).catch((e) => done(e));
      });
  });

  it('should reject invalid login', (done) => {
    request(app)
      .post('/api/users/login')
      .send({
        email: users[1].email,
        password: users[1].password + '1'
      })
      .expect(400)
      .expect((res) => {
        expect(res.headers['x-auth']).toNotExist();
      })
      .end((err, res) => {
        if (err) {
          return done(err);
        }

        User.findById(users[1]._id).then((user) => {
          expect(user.tokens.length).toBe(1);
          done();
        }).catch((e) => done(e));
      });
  });
});

describe('DELETE /api/users/me/token', () => {
  it('should remove auth token on logout', (done) => {
    request(app)
      .delete('/api/users/me/token')
      .set('x-auth', users[0].tokens[0].token)
      .expect(200)
      .end((err, res) => {
        if (err) {
          return done(err);
        }

        User.findById(users[0]._id).then((user) => {
          expect(user.tokens.length).toBe(0);
          done();
        }).catch((e) => done(e));
      });
  });
});
