const gulp = require('gulp'),
    clean = require('gulp-clean'),
    exec = require('child_process').exec,
    runSequence = require('run-sequence'),
    rsync = require('gulp-rsync');


const frontend = 'frontendTM470';
const backend = 'backendTM470';

gulp.task('name', function () {
    console.log('hello');
});

// Clean the dist directory in frontend
gulp.task('clean-dist', function () {
    return gulp.src(frontend + '/dist', {read: false})
        .pipe(clean());
});

// Clean the views directory in backend
gulp.task('clean-views', function () {
    return gulp.src(backend + '/views', {read: false})
        .pipe(clean());
});

// build the frontend
gulp.task('build', function (cb) {
    exec('cd ' + frontend + ';  ng build --base-href=/ --progress false', function (err, stdout, stderr) {
        console.log(stdout);
        console.log(stderr);
        cb(err);
    });
});

// Copy the frontend to the backend views folder.
gulp.task('copy', function () {
    return gulp.src(frontend + '/dist/frontend/*').pipe(gulp.dest(backend + '/views'));
});

// Deploy remotely
gulp.task('deploy', function() {
    return gulp.src(backend + '/views/*')
        .pipe(rsync({
            root: '',
            hostname: 'maikel',
            username: 'ubuntu',
            progress: false,
            destination: '/home/ubuntu'
        }));
});


gulp.task('default', function (callback) {
    runSequence(['clean-dist', 'clean-views'], 'build', 'copy', 'clean-dist',
        'deploy', callback);
});
