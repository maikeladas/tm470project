import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import {Jobapplication} from './jobapplication';
import {AppSettings} from './appsettings';

@Injectable({
  providedIn: 'root'
})
export class JobapplicationService {
  headerJson = {
    'Content-Type': 'application/json',
  };
  jobapplications : Jobapplication[];
  constructor(private http: HttpClient) {

  }

  getApplications(aToken) {
    this.inserToken(aToken);
    return this.http.get<Jobapplication>(AppSettings.API_ENDPOINT + 'jobapplications',
      {headers: this.headerJson, observe: 'response'});
  }

  inserToken(aToken) {
    this.headerJson['x-auth'] = aToken;
  }

  sendJA(aJobApp, aToken) {
    this.inserToken(aToken);
    return this.http.post<Jobapplication>(AppSettings.API_ENDPOINT + 'jobapplications',
      aJobApp, {headers: this.headerJson, observe: 'response'});
  }

  amendJA(aJobApp, aToken) {
    this.inserToken(aToken);
    return this.http.patch<Jobapplication>(AppSettings.API_ENDPOINT + 'jobapplications/' + aJobApp._id,
      aJobApp, {headers: this.headerJson, observe: 'response'});
  }

  getDistance(aLocation, aUserPosctode) {
    return this.http.get('https://maps.googleapis.com/maps/api/distancematrix/json?units=metric&origins=56+Brouncker+Road, W38AQ&destinations=8 Elgin Avenue+W38EL&key=AIzaSyCvk_e8MCvFdvy4Xi8l1jYsWXiedbefNUk',
      {headers: this.headerJson, observe: 'response'});
  }

  deleteJA(aJobApp, aToken) {
    this.inserToken(aToken);
    return this.http.delete<Jobapplication>(AppSettings.API_ENDPOINT + 'jobapplications/' + aJobApp._id,
      {headers: this.headerJson, observe: 'response'});
  }

}
