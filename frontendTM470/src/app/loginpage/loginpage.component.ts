import {Component, OnInit, ViewEncapsulation} from '@angular/core';
import {UserService} from '../user.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-loginpage',
  templateUrl: './loginpage.component.html',
  encapsulation: ViewEncapsulation.None
})
export class LoginpageComponent implements OnInit {
  isSignup = false;
  postcode = '';
  isButtonDisabled: boolean;
  buttonText = '';
  badPassword = false;
  tandcs = false;
  constructor(private userService: UserService, private router: Router) {
  }

  ngOnInit() {
    this.isButtonDisabled = false;
    this.setButton();
  }

  logUser(email, password) {
    if (this.isSignup) {
      // Should create a new user
      this.userService.signup(email, password, this.postcode)
        .subscribe(response => {
          // Should save the token in the service.
          this.userService.storeUser(response);
          // Should route to the list of jobapplications.
          this.router.navigate(['/list']);
        }, error => {
          if (error.error.code === 11000) {
            alert('The username already exist, click on \'already registered\' instead \n if that is you or choose another email');
          }
        });
    } else {
      // Should login the existent user
      this.userService.login(email, password)
        .subscribe(response => {
          this.userService.storeUser(response);
          this.router.navigate(['list']);
        }, error => {
          this.badPassword = true;
        });
    }
  }

  changeToLogin() {
    this.isSignup = !(this.isSignup);
    this.setButton();
  }

  setButton() {
    if (this.isSignup) {
      this.isButtonDisabled = true;
      this.buttonText = 'Sign Up';
    } else {
      this.isButtonDisabled = false;
      this.buttonText = 'Log In';
    }
  }

  showTerms() {
    this.router.navigate(['terms']);
  }

  validate(email, password) {
    this.badPassword = false;
    const theEmail = email.value;
    const thePass = password.value;
    if (!(this.isSignup)) { this.isButtonDisabled = false; return; }
    if (this.isValidEmail(theEmail) && thePass.length > 6 && this.postcode.length > 4 && this.hasAcceptedTandCs() === true) {
      this.isButtonDisabled = false;
    } else {
      this.isButtonDisabled = true;
    }

  }

  isValidEmail(someEmail) {
    return (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(someEmail));
  }

  hasAcceptedTandCs() {
    return this.tandcs;
  }
}
