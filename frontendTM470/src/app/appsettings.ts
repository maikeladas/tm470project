import {environment} from '../environments/environment'

export class AppSettings {
    public static API_ENDPOINT = environment.production === true ? 'https://www.jobapplicationtracker.site/api/'
      : 'http://localhost:3002/api/';
}
