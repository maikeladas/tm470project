import {Component, OnInit, ViewEncapsulation} from '@angular/core';
import {UserService} from './user.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  encapsulation: ViewEncapsulation.None
})
export class AppComponent implements OnInit{
  constructor (private userService: UserService, private router: Router) {}
  ngOnInit() {
    if (this.userService.checkLoggedIn() === false) {
    } else {
      // Log the user and
      this.userService.logUser();
      this.router.navigate(['/list']);
      // Send the user to the list page.
    }
  }

}
