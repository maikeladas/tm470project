import {Component, OnInit, TemplateRef, ViewEncapsulation} from '@angular/core';
import {UserService} from '../user.service';
import {User} from '../user.model';
import {Router} from '@angular/router';
import {JobapplicationService} from '../jobapplication.service';
import {Jobapplication} from '../jobapplication';
import {BsModalService} from 'ngx-bootstrap';
import {BsModalRef} from 'ngx-bootstrap/modal/bs-modal-ref.service';

@Component({
  selector: 'app-listpage',
  templateUrl: './listpage.component.html',
  encapsulation: ViewEncapsulation.None
})
export class ListpageComponent implements OnInit {
  modalRef: BsModalRef;
  theUser: User;
  isSortedByDate = true;
  aJobApp = new Jobapplication('', '', '', '');
  theJAs: Jobapplication[];

  constructor(private modalService: BsModalService, private userService: UserService, private router: Router,
              private jaService: JobapplicationService) {
  }

  ngOnInit() {
    this.getUser();
    this.getList();
  }

  getUser() {
    if (this.userService.checkLoggedIn() === false) {
      this.router.navigate(['login'])
    }
    this.theUser = this.userService.getUser();
  }

  logout() {
    // Delete the user locally
    this.userService.logout();
    // Go back to AppComponent
    this.router.navigate(['/']);
  }

  getList() {
    this.jaService.getApplications(this.theUser.token)
      .subscribe((response) => {
        this.theJAs = Jobapplication.parseJobApplications(response.body);
        if (this.isSortedByDate === true) {
          this.sortByDate();
        } else {
          this.sortByDistance();
        }
      });
  }

  openWindow(template: TemplateRef<any>, ajobapp, text) {
    if (text === 'CREATE') {
      this.aJobApp = new Jobapplication('', '', '', '');
    } else {
      this.aJobApp = ajobapp;
    }
    this.modalRef = this.modalService.show(template);
  }

  sendJA() {
    this.jaService.sendJA(this.aJobApp, this.theUser.token)
      .subscribe(response => {
        this.getList();
        this.modalRef.hide();
      });
  }

  amendJA() {
    this.jaService.amendJA(this.aJobApp, this.theUser.token)
      .subscribe(response => {
        this.getList();
        this.modalRef.hide();
      });
  }

  formatNice(aDistance) {
    if (aDistance === '-1') {
      return 'n/a';
    }
    return Math.round(aDistance / 1000).toString() + ' kms';
  }

  sortByDate() {
    this.isSortedByDate = true;
    // This needs to be fixed
    this.theJAs.sort(function (a, b) {
      const left = (a.dateApplied === undefined ? new Date('2015-01-01') : a.dateApplied);
      const right = (b.dateApplied === undefined ? new Date('2015-01-01') : b.dateApplied);
      const result = new Date(right).getTime() - new Date(left).getTime();
      return result;
    });
  }

  sortByDistance() {
    this.isSortedByDate = false;
    this.theJAs.sort(function (a, b) {
      const left = parseInt(a.distance, 10);
      const right = parseInt(b.distance, 10);

      if (left > right) {
        return 1;
      }
      if (left < right) {
        return -1;
      }

      if (left === right) {
        return 0;
      }
    });
  }

  deleteJA() {
    this.jaService.deleteJA(this.aJobApp, this.theUser.token)
      .subscribe(response => {
        this.getList();
        this.modalRef.hide();
      });
  }
}
