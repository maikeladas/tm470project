import {Component, EventEmitter, Input, OnInit, Output, ViewEncapsulation} from '@angular/core';

@Component({
  selector: 'app-topbar',
  templateUrl: './topbar.component.html',
  encapsulation: ViewEncapsulation.None
})
export class TopbarComponent implements OnInit {
  @Input() email: string;
  @Output() doLogout: EventEmitter<any>;
  constructor() {
    this.doLogout = new EventEmitter<any>();
  }

  ngOnInit() {
  }

  logout() {
    this.doLogout.emit();
  }

}
