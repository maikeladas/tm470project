import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule} from '@angular/forms';
import { RouterModule, Routes } from '@angular/router';
import { HttpClientModule} from '@angular/common/http';

// Bootstrap modules.
import { BsDropdownModule } from 'ngx-bootstrap/dropdown';
import { TooltipModule } from 'ngx-bootstrap/tooltip';
import { ModalModule } from 'ngx-bootstrap/modal';

import { AppComponent } from './app.component';
import { LoginpageComponent } from './loginpage/loginpage.component';
import { ListpageComponent } from './listpage/listpage.component';
import { TopbarComponent } from './listpage/topbar/topbar.component';
import { TermsComponent } from './terms/terms.component';

const appRoutes: Routes = [
  { path: '', component: LoginpageComponent},
  { path: 'list', component: ListpageComponent },
  { path: 'terms', component: TermsComponent },
  { path: '**', component: LoginpageComponent }
];

@NgModule({
  declarations: [
    AppComponent,
    LoginpageComponent,
    ListpageComponent,
    TopbarComponent,
    TermsComponent
  ],
  imports: [
    BsDropdownModule.forRoot(),
    TooltipModule.forRoot(),
    ModalModule.forRoot(),
    BrowserModule,
    HttpClientModule,
    RouterModule.forRoot(appRoutes, {enableTracing: false}),
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
