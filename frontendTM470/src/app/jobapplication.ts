export class Jobapplication {
  constructor(public roleName: string, public companyName: string, public notes: string, public location: string,
              public dateApplied?: Date, public referenceLetterName?: string, public cvUsedName?: string, public _creator?: string,
              public _id?: string, public distance?: string) {
  }

  static parseJobApplications(body) {
    const formattedJobApps = [];
    const jobapps = body.jobapplications;
    for (const someJAP of jobapps) {
      const anAPP = new Jobapplication(someJAP.roleName, someJAP.companyName, someJAP.notes, someJAP.location, someJAP.dateApplied,
        someJAP.referenceLetterName, someJAP.cvUsedName, someJAP._creator, someJAP._id, someJAP.distance);
      formattedJobApps.push(anAPP);
    }
    return formattedJobApps;
  }
}
