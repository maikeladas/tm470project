import { Injectable } from '@angular/core';
import { HttpClient} from '@angular/common/http';
import {User} from './user.model';
import {catchError} from 'rxjs/operators';
import { Observable } from 'rxjs';
import {AppSettings} from './appsettings';


@Injectable({
  providedIn: 'root'
})
export class UserService {
  headerJson = {
    'Content-Type': 'application/json',
  };
  private theUser: User;

  constructor(private http: HttpClient) { }

signup(email, password, postcode) {

    //Notice that password cannot be less than some number of characters. s
    const userData = {
      'email': email,
      'password': password,
      'postcode': postcode
    };
    return this.http.post<User>(AppSettings.API_ENDPOINT + 'users',
      userData, {headers: this.headerJson, observe: 'response'});
}

login(email, password) {
  const userData = {
    'email': email,
    'password': password
  };
    return this.http.post<User>(AppSettings.API_ENDPOINT + 'users/login'
      , userData, {headers: this.headerJson, observe: 'response'});
}

storeUser(response) {
    this.theUser = new User(response.body.email, response.body._id, response.headers.get('x-auth'), response.body.postcode);
    localStorage.setItem('User', JSON.stringify(this.theUser));

}

  inserToken(aToken) {
    this.headerJson['x-auth'] = aToken;
  }

  getUser() {
    return this.theUser;
  }

  checkLoggedIn() {
    if (JSON.parse(localStorage.getItem('User')) === null) {
      return false;
    } else {
      this.logUser();
      return true;
    }

  }
  logUser() {
    this.theUser = JSON.parse(localStorage.getItem('User'));
    //Should also delete the token
  }

  logout() {
    localStorage.removeItem('User');
    this.theUser = null;
  }
}
