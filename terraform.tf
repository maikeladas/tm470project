provider "aws" {
  access_key = "AKIAJAS5SACRIN2YYBXA"
  secret_key = "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"
  region     = "eu-west-2"

}


# Creates a VPC to launch my instances
resource "aws_vpc" "470vpc" {
  cidr_block = "10.0.0.0/16"
  tags {
  Name = "470vpc"
}
}

# Create an internet gateway to give my subnets access to the outside world
resource "aws_internet_gateway" "default" {
  vpc_id = "${aws_vpc.470vpc.id}"
  tags {
  Name = "470gateway"
}
}

# Grant the VPC internet access on its main route table
resource "aws_route" "internet_access" {
  route_table_id         = "${aws_vpc.470vpc.main_route_table_id}"
  destination_cidr_block = "0.0.0.0/0"
  gateway_id             = "${aws_internet_gateway.default.id}"
}

# Create one first subnet to launch my instances into.
resource "aws_subnet" "first" {
  vpc_id                  = "${aws_vpc.470vpc.id}"
  cidr_block              = "10.0.1.0/24"
  map_public_ip_on_launch = false
  availability_zone = "eu-west-2a"
}

resource "aws_subnet" "second" {
  vpc_id                  = "${aws_vpc.470vpc.id}"
  cidr_block              = "10.0.2.0/24"
  map_public_ip_on_launch = false
  availability_zone = "eu-west-2b"
}

resource "aws_subnet" "third" {
  vpc_id                  = "${aws_vpc.470vpc.id}"
  cidr_block              = "10.0.3.0/24"
  map_public_ip_on_launch = false
  availability_zone = "eu-west-2c"
}

# A security group for the ELB so it is accessible via the web
resource "aws_security_group" "elb" {
  name        = "elb_security_group"
  description = "Used in the terraform"
  vpc_id = "${aws_vpc.470vpc.id}"

  # HTTPs access from anywhere
  ingress {
    from_port   = 443
    to_port     = 443
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  # outbound internet access
  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

# My default security group to access the instances over SSH and HTTP...for now.
resource "aws_security_group" "470instances" {
  name        = "launch_configuration_security_group"
  description = "Used in the terraform"
  vpc_id = "${aws_vpc.470vpc.id}"

  # SSH access from anywhere
  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  # HTTP access from the VPC
  ingress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  # outbound internet access
  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

resource "aws_elb" "default" {
  name = "jobapplicationtracker"
  security_groups = ["${aws_security_group.elb.id}"]
  cross_zone_load_balancing   = false
  subnets = ["${aws_subnet.first.id}", "${aws_subnet.second.id}", "${aws_subnet.third.id}"]
  # Accepts petitions from port 443 but redirects them to port 80 of the instances.
  listener {
    instance_port     = 80
    instance_protocol = "http"
    lb_port           = 443
    lb_protocol       = "https"
    ssl_certificate_id = "arn:aws:acm:eu-west-2:640999260392:certificate/6f8f2162-c868-4652-90df-0b0f0f2e152b"
  }
}

resource "aws_key_pair" "auth" {
  key_name   = "terraform"
  public_key = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAACAQDAYl4mUamUBhz+vcjAyBUUboAioPvSZLhQy28DKTuucB/21Ck/V+Rchr2kXkfUP97tEqo3PYr2HMqE2UmlNhCvOf6xnTGuPqFBJDizD7qRe2Mj/95sjQ0ckZAbyk8Vn/ukP9V4chepRkejHnWJIBqesDPDG8AWQpwXtpAz7VFwJgymMmrurotca//JfOa6rnHbPmVyadOJ/jZ390kwCWPaDv+0cEyCbTDhXM38oe6RBE32+VqMgclYpfYF59mUGZMP6nyuWcoT+zyMIbjGVq/6O0teDQv4p5+H1/gPzbX5XB4pRKmOZ7ngUoeJ8TJCpXf8J54aOjs7v9AJptQhP1U4WHP2JEULlkeCvM8IH/Ne1RCyJTsUfS7B0CGAkNe0qGfCM28wXUCIKXBsU1+qtZUJVFL1EuO3PbxQuvQDMj7J1XJmUpJuFjZeyzZteOhHiKSMjaCiDVelRBdvgnV9BWX4smJFdf/UylWJgwmvZbfWe5oIyXxexWT6zNMGDWs0sHIyeOUlsmPKvfWdAtvFdqlMZVcsO47XlifDpvfdYGpfw46ePmFkfmvMLmC+33sQMkOC14LMjkTho44PaHn5uTn1wh2nhuR0knl0pSWwxKmdA+jWcsTzslRnpayUlFbykaZ0erGGXVA6+uB4lPWmZZVgJxCIorkSo/Wm2+MjWu/aQw== maikel@maikel.uk"
}

resource "aws_launch_configuration" "default" {
  name_prefix   = "tm470-"
  image_id      = "ami-34f61e53"
  instance_type = "t2.micro"
  key_name = "${aws_key_pair.auth.key_name}"
  security_groups = ["${aws_security_group.470instances.id}"]

  lifecycle {
    create_before_destroy = true
  }
}

resource "aws_autoscaling_group" "default" {
  vpc_zone_identifier = ["${aws_subnet.first.id}", "${aws_subnet.second.id}", "${aws_subnet.third.id}"]
  desired_capacity = 1
  max_size = 3
  min_size = 1
  health_check_type         = "ELB"
  load_balancers = ["${aws_elb.default.name}"]
  launch_configuration = "${aws_launch_configuration.default.name}"
}

resource "aws_lb_target_group" "default" {
  name     = "terraform-target-group"
  port     = 80
  protocol = "HTTP"
  vpc_id   = "${aws_vpc.470vpc.id}"
}

resource "aws_autoscaling_attachment" "default" {
  autoscaling_group_name = "${aws_autoscaling_group.default.id}"
  elb                    = "${aws_elb.default.id}"
  alb_target_group_arn   = "${aws_lb_target_group.default.arn}"
}

data "aws_route53_zone" "primary" {
  name         = "jobapplicationtracker.site"
  private_zone = false
}


resource "aws_route53_record" "www" {
  zone_id = "${data.aws_route53_zone.primary.zone_id}"
  name    = "www.${data.aws_route53_zone.primary.name}"
  type    = "A"

  alias {
    name                   = "${aws_elb.default.dns_name}"
    zone_id                = "${aws_elb.default.zone_id}"
    evaluate_target_health = true
  }
}

# My policy to scale up or down
resource "aws_autoscaling_policy" "policyUp" {
  name                   = "terraform-policyUp-TM470"
  adjustment_type        = "ChangeInCapacity"
  autoscaling_group_name = "${aws_autoscaling_group.default.name}"
  policy_type            = "TargetTrackingScaling"

  target_tracking_configuration {
  predefined_metric_specification {
    predefined_metric_type = "ASGAverageCPUUtilization"
  }
  target_value = 50.0
}
}

output "address" {
  value = "${aws_elb.default.dns_name}"
}
